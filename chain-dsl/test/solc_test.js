const {expect} = require('./helpers')
const solc = require('../solc')

describe('solc', () => {
    describe('planFromSources', () => {
        it('works', () => {
            const plan = solc.planFromSources(['dir/Contract.sol'])

            expect(plan)
                .have.property('settings')
                .have.property('outputSelection')
                .have.property('Contract.sol')
                .have.property('*')
                .containSubset(['abi', 'evm.bytecode'])

            expect(plan)
                .have.property('sources')
                .have.property('Contract.sol')
                .have.property('urls')
                .eql(['dir/Contract.sol'])
        })

        const sampleExpectedValue = {
            "language": "Solidity",
            "settings": {
                "remappings": [],
                "optimizer": {
                    "enabled": false,
                    "runs": 500
                },
                "outputSelection": {
                    "Contract.sol": {
                        "*": [
                            "metadata",
                            "abi",
                            "evm.bytecode",
                            "evm.sourceMap"
                        ]
                    }
                }
            },
            "sources": {
                "Contract.sol": {
                    "urls": [
                        "dir/Contract.sol"
                    ]
                }
            }
        }
    })

    describe('flattenJsonOutput', () => {
        it('flattens', () => {
            // `evm.bytecode.object`, `evm.bytecode.opcodes` and `metadata` are shortened
            const Foo = {
                "abi": [
                    {
                        "constant": true,
                        "inputs": [],
                        "name": "bar",
                        "outputs": [
                            {
                                "name": "x",
                                "type": "uint256"
                            }
                        ],
                        "payable": false,
                        "stateMutability": "pure",
                        "type": "function"
                    },
                    {
                        "inputs": [],
                        "payable": false,
                        "stateMutability": "nonpayable",
                        "type": "constructor"
                    }
                ],
                "evm": {
                    "bytecode": {
                        "linkReferences": {},
                        "object": "6060604052...",
                        "opcodes": "PUSH1 0x60 PUSH1 0x40 MSTORE ...",
                        "sourceMap": "25:127:0:-;;;44:30;;;;;;;;25:127;;;;;;"
                    }
                },
                "metadata": "{\"compiler\":{\"version\":\"0.4.20+commit.3155dd80\"},\"language\":\"Solidity\",\"output\":{\"abi\":[\"<reducted>\"],\"devdoc\":{\"methods\":{}},\"userdoc\":{\"methods\":{}}},\"settings\":{\"compilationTarget\":{\"Foo.sol\":\"Foo\"},\"libraries\":{},\"optimizer\":{\"enabled\":false,\"runs\":500},\"remappings\":[]},\"sources\":{\"Foo.sol\":{\"keccak256\":\"0xc65c...45067\",\"urls\":[\"bzzr://dbf5...2240\"]}},\"version\":1}\n"
            }

            const solcJsonOutput = {
                "contracts": {
                    "Foo.sol": {
                        "Foo": Foo
                    }
                },
                "sources": {
                    "Foo.sol": {
                        "id": 0
                    }
                }
            }

            expect(solc.flattenJsonOutput(solcJsonOutput
            )).eql({"Foo": {...Foo, "contractType": "Foo"}})
        })

        it('works with empty contract list', () => {
            const errors = [{
                component: 'general',
                formattedMessage: 'Cannot import url ("src/Foo.sol"): File outside of allowed directories.',
                message: 'Cannot import url ("src/Foo.sol"): File outside of allowed directories.',
                severity: 'error',
                type: 'IOError'
            }]

            const solcJsonOutput = {
                contracts: {},
                errors,
                sources: {}
            }

            expect(solc.flattenJsonOutput(solcJsonOutput)).eql({errors})
        })
    })
})
