/* eslint-env mocha */
/* eslint no-unused-expressions: off */

import sinon from 'sinon'
import { expect } from 'chain-dsl/test/helpers'
import * as MessageBroker from '../../src/dex/messaging/MessageBroker.js'
import { makeChannel } from '../../src/dex/utils'
import { Message } from '../../src/dex/messaging/Message'

const { utils: { utf8ToHex } } = Web3

describe('Message Broker', function () {
  this.timeout(1000)
  this.slow(1000)

  let web3,
    OAX,
    WETH,
    broker,
    tokenPair,
    sendChannel,
    receiveChannel,
    shhPostSpy,
    messageToSend

  before(async () => {
    OAX = {
      name: 'Open Asset Exchange',
      symbol: 'OAX',
      address: Web3.utils.randomHex(20)
    }
    WETH = {
      name: 'Wrapped Ether',
      symbol: 'WETH',
      address: Web3.utils.randomHex(20)
    }

    tokenPair = {
      base: OAX,
      quote: WETH
    }

    sendChannel = makeChannel()
    receiveChannel = makeChannel()

    messageToSend = Message({
      type: 'order',
      data: { message: 'hello' },
      tokens: Web3.utils.sha3('some-hash')
    })
  })

  beforeEach(async () => {
    broker = MessageBroker.create({
      web3,
      tokenPair,
      sendChannel,
      receiveChannel,
      pollInterval: 20
    })

    shhPostSpy = sinon.spy(web3.shh, 'post')

    broker = await MessageBroker.start(broker, { secret: 'test' })
  })

  afterEach(async function () {
    await MessageBroker.stop(broker)
    shhPostSpy.restore()
  })

  context('with HTTP provider', function () {
    before(function () {
      web3 = new Web3(devChainUrl)
    })

    runTestSuite()
  })

  context('with WS provider', function () {
    before(function () {
      web3 = new Web3(devChainWsUrl)
    })

    runTestSuite()

    after(function () {
      web3.currentProvider.connection.close()
    })
  })

  function runTestSuite () {
    it('.start creates a symmetric key on Whisper node', async () => {
      expect(await broker.web3.shh.hasSymKey(broker.symKeyID)).to.be.true
    })

    it('message sent through send channel reaches Web3 Whisper', async function () {
      const receivedMessage = new Promise(resolve => {
        broker.sendMessageConfirmation.subscribe(msg => resolve(msg))
      })

      sendChannel.next(messageToSend)

      expect((await receivedMessage).message).to.eql(messageToSend)
      expect(shhPostSpy.calledOnce).equal(true)
    })

    it('message sent is received through receive channel', async function () {
      const receivedMessage = new Promise(resolve => {
        broker.receiveChannel.subscribe(msg => resolve(msg))
      })

      sendChannel.next(messageToSend)

      expect(await receivedMessage).to.eql(messageToSend)
    })

    context('Exception handling', function () {})

    it('retries message sending when PoW is low', async function () {
      const shhInfo = await web3.shh.getInfo()
      broker.powTarget = shhInfo.minPow * 0.9

      const receivedMessage = new Promise(resolve => {
        broker.sendMessageConfirmation.first().subscribe(msg => resolve(msg))
      })

      sendChannel.next(messageToSend)

      expect((await receivedMessage).message).to.eql(messageToSend)
      expect((await receivedMessage).error).to.be.undefined
      expect(shhPostSpy.calledTwice).equal(true)
    })

    it('throws exception after exhausting retries', async function () {
      shhPostSpy.restore()

      const shhInfo = await web3.shh.getInfo()
      broker.powTarget = shhInfo.minPow / 10
      broker.powRetry = 2

      const mock = sinon.mock(web3.shh)
      mock
        .expects('post')
        .atLeast(broker.powRetry + 1)
        .rejects(Error('Returned error: message rejected, PoW too low'))

      const receivedMessage = new Promise(resolve => {
        broker.sendMessageConfirmation.first().subscribe(msg => resolve(msg))
      })

      sendChannel.next(messageToSend)

      expect((await receivedMessage).message).to.eql(messageToSend)
      expect((await receivedMessage).error.message).to.match(/PoW too low/)
      mock.verify()
      mock.restore()
    })

    context('Malformatted DEX message handling', function () {
      let badWhisperMessage, getBrokerEventsAsync

      before(() => {
        badWhisperMessage = {
          payload: '',
          ttl: 70,
          topic: MessageBroker.TOPICS.OAX_ORDERS,
          powTarget: 3,
          powTime: 10,
          symKeyID: broker.symKeyID
        }

        getBrokerEventsAsync = function (broker) {
          return new Promise(resolve => {
            broker.events.first().subscribe(msg => resolve(msg))
          })
        }
      })

      it('handles empty payload', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        await web3.shh.post(badWhisperMessage)

        expect((await errorMessage).error).to.match(
          /Unexpected end of JSON input/
        )
      })

      it('handles non-JSON payload', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        await web3.shh.post({ ...badWhisperMessage, payload: utf8ToHex('hello') })

        expect((await errorMessage).error).to.match(
          /Unexpected token h in JSON at position 0/
        )
      })

      it('handles incorrect message `type`', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        const message = {
          ...messageToSend,
          type: 'wrong-type'
        }
        const payload = utf8ToHex(JSON.stringify(message))
        await web3.shh.post({ ...badWhisperMessage, payload })

        expect((await errorMessage).error).to.match(
          /Expected a value of type `"order" | "rfob"` for `type`/
        )
      })

      it('handles incorrect `tokens` field', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        const message = {
          ...messageToSend,
          tokens: '0xwronghash'
        }
        const payload = utf8ToHex(JSON.stringify(message))
        await web3.shh.post({ ...badWhisperMessage, payload })

        expect((await errorMessage).error).to.match(
          /Expected a value of type `hash` for `tokens`/
        )
      })

      it('handles incorrect `version` field', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        const message = {
          ...messageToSend,
          version: 'old-version'
        }
        const payload = utf8ToHex(JSON.stringify(message))
        await web3.shh.post({ ...badWhisperMessage, payload })

        expect((await errorMessage).error).to.match(
          /TypeError: Expected a value of type `number`/
        )
      })

      it('handles incorrect `data` field', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        const message = {
          ...messageToSend,
          data: null
        }
        const payload = utf8ToHex(JSON.stringify(message))
        await web3.shh.post({ ...badWhisperMessage, payload })

        expect((await errorMessage).error).to.match(
          /Expected a value of type `object` for `data`/
        )
      })

      it('handles incorrect `version` field', async function () {
        const errorMessage = getBrokerEventsAsync(broker)

        const message = {
          ...messageToSend,
          version: 'old-version'
        }
        const payload = utf8ToHex(JSON.stringify(message))
        await web3.shh.post({ ...badWhisperMessage, payload })

        expect((await errorMessage).error).to.match(
          /TypeError: Expected a value of type `number`/
        )
      })
    })

    it('removes message filters on stop', async function () {
      const filters = broker.topicFilters
      await MessageBroker.stop(broker)

      const results = await Promise.all(
        filters.map(async filter => {
          try {
            return await broker.web3.shh.getFilterMessages(filter)
          } catch (err) {
            return 'deleted'
          }
        })
      )

      expect(R.all(R.equals('deleted'), results)).to.be.true
    })
  }
})
