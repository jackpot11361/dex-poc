/* eslint-env mocha */

import { expect } from 'chain-dsl/test/helpers'
import { VuexMutationsPayload } from '../../../src/dex/streams/VuexMutationsPayload'

describe('VuexMutationsPayload', () => {
  let store

  beforeEach(() => {
    store = new Vuex.Store({
      mutations: {
        dummyMutation (state, payload) {},
        dummyMutation2 (state, payload) {}
      },
      strict: true
    })
  })

  it('pushes a mutation downstream', async () => {
    const payload = await new Promise(resolve => {
      VuexMutationsPayload(store, ['dummyMutation'])
        .first()
        .subscribe(val => resolve(val))

      store.commit('dummyMutation', 'dummy-payload')
    })

    expect(payload).to.eql('dummy-payload')
  })

  it('supports multiple mutations', async () => {
    const receivedPayloads = []

    await new Promise(resolve => {
      VuexMutationsPayload(store, ['dummyMutation', 'dummyMutation2'])
        .take(2)
        .subscribe({
          next: payload => receivedPayloads.push(payload),
          complete: resolve
        })

      store.commit('dummyMutation', 'dummy-payload')
      store.commit('dummyMutation2', 'dummy-payload-2')
    })

    expect(receivedPayloads.includes('dummy-payload'))
    expect(receivedPayloads.includes('dummy-payload-2'))
  })
})
