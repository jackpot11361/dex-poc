/* eslint-env mocha */

import { expect } from 'chain-dsl/test/helpers'
import { VuexMutations } from '../../../src/dex/streams/VuexMutations'

describe('VuexMutations', () => {
  let store

  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        dummyProperty: 'dummy-value'
      },
      mutations: {
        dummyMutation (state, payload) {},
        dummyMutation2 (state, payload) {}
      },
      strict: true
    })
  })

  it('pushes a mutation payload downstream', async () => {
    const commit = await new Promise(resolve => {
      VuexMutations(store, ['dummyMutation'])
        .first()
        .subscribe(commit => resolve(commit))

      store.commit('dummyMutation', 'dummy-payload')
    })

    expect(commit.mutation.type).to.eql('dummyMutation')
    expect(commit.mutation.payload).to.eql('dummy-payload')
    expect(commit.state).to.eql({dummyProperty: 'dummy-value'})
  })

  it('supports multiple mutations', async () => {
    const receivedMutations = []

    await new Promise(resolve => {
      VuexMutations(store, ['dummyMutation', 'dummyMutation2'])
        .take(2)
        .subscribe({
          next: (commit) => receivedMutations.push(commit),
          complete: () => resolve()
        })

      store.commit('dummyMutation', 'dummy-payload')
      store.commit('dummyMutation2', 'dummy-payload-2')
    })

    expect(receivedMutations).to.deep.include({
      mutation: {
        type: 'dummyMutation',
        payload: 'dummy-payload'
      },
      state: {
        dummyProperty: 'dummy-value'
      }
    })

    expect(receivedMutations).to.deep.include({
      mutation: {
        type: 'dummyMutation2',
        payload: 'dummy-payload-2'
      },
      state: {
        dummyProperty: 'dummy-value'
      }
    })
  })
})
