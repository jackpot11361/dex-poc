/* eslint-env mocha */

import {expect} from 'chain-dsl/test/helpers'
import {waitForConnection} from 'chain-dsl'
import {NodejsContractLoader as ContractLoader} from '../../src/NodejsContractLoader'
import * as Contracts from '../../src/dex/Contracts'

describe('Contracts', function () {
  it('.load', async () => {
    const web3 = new Web3(devChainUrl)
    await waitForConnection(web3)

    const loader = ContractLoader(await web3.eth.net.getId())
    const contracts = await Contracts.load(loader, web3)

    // expect(contracts).include.keys(['oax', 'weth', 'swimusd'])
    expect(contracts).include.keys(['oax', 'weth'])
    const methodNames = Object.keys(contracts.weth.methods)
    expect(methodNames).contains('totalSupply')
    expect(methodNames).contains('totalSupply()')
  })
})
