/* eslint-env mocha */

import Storage from 'dom-storage'
import { expect } from '../../../chain-dsl/test/helpers'

import sinon from 'sinon'
import * as ContractLog from '../../src/dex/ContractLog'
import {NodejsContractLoader as ContractLoader} from '../../src/NodejsContractLoader'
import * as Contracts from '../../src/dex/Contracts'
import { config } from '../../env/dev'

describe('Contract Log', () => {
  let web3, contracts, mock, contractLog, storage, mockWeb3

  before(async () => {
    web3 = new Web3(devChainUrl)
    const loader = ContractLoader(await web3.eth.net.getId())
    contracts = await Contracts.load(loader, web3)
  })

  beforeEach(async () => {
    storage = new Storage(null, {strict: true})

    contractLog = ContractLog.create({
      web3,
      exchange: contracts.xchg,
      storage,
      netId: config.networkId,
      pollInterval: 20,
      historySize: 10
    })
  })

  afterEach(() => {
    if (mock) { mock.restore() }
    ContractLog.stop(contractLog)
  })

  it('polls event from a contract and token pair', async () => {
    const exchange = contracts.xchg
    const dummyEvent = {'event': 'LogFill'}

    mock = sinon.mock(exchange)
      .expects('getPastEvents')
      .atLeast(1)
      .resolves([dummyEvent])

    await ContractLog.start(contractLog)

    const event = await new Promise(resolve => {
      contractLog.events.first().subscribe(resolve)
    })

    expect(event).to.eql(dummyEvent)
    mock.verify()
  })

  context('Log events window', function () {
    afterEach(function () {
      if (!R.isNil(mockWeb3)) {
        mockWeb3.restore()
        mockWeb3 = null
      }
    })

    context('when the current block number is <= historySize', function () {
      it('starts monitoring from genesis block', async function () {
        mockWeb3 = sinon.mock(web3.eth)
          .expects('getBlockNumber')
          .atLeast(1)
          .resolves(0)

        await ContractLog.start(contractLog)

        expect(contractLog.fromBlock).to.eql(0)
        mockWeb3.verify()
      })
    })

    context('when the current block number is > historySize', function () {
      context('last seen block is outside of the historical block range', function () {
        it('sets from block to last seen block', async function () {
          mockWeb3 = sinon.mock(web3.eth)
            .expects('getBlockNumber')
            .atLeast(1)
            .resolves(100)

          storage.setItem(`lastSeenBlock-1337`, '80')

          await ContractLog.start(contractLog)

          expect(contractLog.fromBlock).to.eql(80)
          mockWeb3.verify()
        })
      })

      context('last seen block is within the historical block range', function () {
        it('sets from block to start of historical block range', async function () {
          mockWeb3 = sinon.mock(web3.eth)
            .expects('getBlockNumber')
            .atLeast(1)
            .resolves(100)

          storage.setItem(`lastSeenBlock-1337`, '95')

          await ContractLog.start(contractLog)

          expect(contractLog.fromBlock).to.eql(90)
          mockWeb3.verify()
        })
      })
    })
  })

  it('fromBlock param updates to the latest block in the events')
})
