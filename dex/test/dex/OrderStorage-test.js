/* eslint-env mocha */

import { TestScheduler } from '@kwonoj/rxjs-testscheduler-compat'

import { expect } from 'chain-dsl/test/helpers'
import { address, waitForConnection } from 'chain-dsl'
import { ZERO_ADDR, now } from 'chain-dsl/es6'
import Storage from 'dom-storage'
import { NodejsContractLoader as ContractLoader } from '../../src/NodejsContractLoader'
import * as Order from '../../src/dex/Order'
import * as Orderbook from '../../src/dex/Orderbook'
import * as Contracts from '../../src/dex/Contracts'
import * as Demo from '../../src/dex/Demo'
import * as OrderStorage from '../../src/dex/OrderStorage'
import { makeChannel } from '../../src/dex/utils'
import { OrderExpiry } from '../../src/dex/streams/OrderExpiry'

describe('OrderStorage', () => {
  let store, orderbook, storage, clock, web3, contracts, order, OAX, WETH, tokenPair
  const netId = 9

  before(async () => {
    web3 = new Web3(devChainUrl)
    await waitForConnection(web3)
    const [DEPLOYER, ALICE, BOB] = await web3.eth.getAccounts()
    const loader = ContractLoader(await web3.eth.net.getId())
    const deployedContracts = await Contracts.load(loader, web3)
    ;({contracts} = await Demo.ensure(web3, deployedContracts,
      {DEPLOYER, ALICE, BOB}))
    const {oax, weth} = contracts
    OAX = {
      name: 'Open Asset Exchange',
      symbol: 'OAX',
      address: address(oax)
    }
    WETH = {
      name: 'Wrapped Ether',
      symbol: 'WETH',
      address: address(weth)
    }

    tokenPair = {
      base: OAX,
      quote: WETH
    }

    order = Order.create({
      address: ZERO_ADDR,
      sig: Web3.utils.randomHex(20),
      buy: {amount: D`0.000001`, token: OAX},
      sell: {amount: D`0.000001`, token: WETH}
    })

    order = Order.withHash(contracts, order)
  })

  beforeEach(async () => {
    store = new Vuex.Store({strict: true})
    storage = new Storage(null, {strict: true})
    orderbook = Orderbook.create({
      store,
      storage,
      netId,
      sendMessageChannel: makeChannel(),
      receiveMessageChannel: makeChannel(),
      tokenPair
    })
    orderbook = Orderbook.start(orderbook)
  })

  afterEach(() => { if (orderbook) orderbook = Orderbook.stop(orderbook) })

  afterEach(() => {
    if (clock) {
      clock.restore()
      clock = null
    }
  })

  it('saved after an order is added', () => {
    Orderbook.insert(orderbook, order)
    const storedOrderbook = OrderStorage.load(orderbook.orderStorage)
    expect(storedOrderbook).to.eql([order])
  })

  it('saved after an order is removed', () => {
    Orderbook.insert(orderbook, order)
    Orderbook.remove(orderbook, order)
    const storedOrderbook = OrderStorage.load(orderbook.orderStorage)
    expect(storedOrderbook).to.eql([])
  })

  it('saved after an order is updated', () => {
    Orderbook.insert(orderbook, order)
    const newOrder = Object.assign({}, order, {buy: {amount: D`0.000002`, token: OAX}})
    Orderbook.update(orderbook, newOrder)
    const storedOrderbook = OrderStorage.load(orderbook.orderStorage)
    expect(storedOrderbook).to.eql([newOrder])
  })

  it('restores orders on startup', async () => {
    Orderbook.insert(orderbook, order)

    // Simulate browser restart
    Orderbook.stop(orderbook)
    store = new Vuex.Store({})
    orderbook = Orderbook.create({
      store,
      storage,
      netId,
      tokenPair,
      sendMessageChannel: makeChannel(),
      receiveMessageChannel: makeChannel()
    })
    orderbook = Orderbook.start(orderbook)

    expect(Orderbook.contains(orderbook, order)).equal(true)
  })

  it('orders restored on startup expires', async () => {
    const scheduler = new TestScheduler()

    const orderExpiringSoon = {...order, exp: now() + 1}
    Orderbook.insert(orderbook, orderExpiringSoon)

    // Simulate restarting the client application where the Vuex store will be empty
    orderbook = Orderbook.stop(orderbook)

    store = new Vuex.Store({strict: true})
    orderbook = Orderbook.create({
      store,
      storage,
      netId,
      tokenPair,
      sendMessageChannel: makeChannel(),
      receiveMessageChannel: makeChannel()
    })

    orderbook.orderExpiry = OrderExpiry(orderbook.orderExpirySource, scheduler)

    orderbook = Orderbook.start(orderbook)
    scheduler.advanceTo(1000)

    expect(Orderbook.contains(orderbook, orderExpiringSoon)).equal(false)
  })
})
