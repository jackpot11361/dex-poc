/* eslint-env mocha */

import {expect} from 'chain-dsl/test/helpers'

const globals = [
  'Web3', 'Eth', 'R', 'Rx', 'Vuex', 'Vue', 'BigNumber', 'D',
  'clone', 'merge', 'start', 'stop'
]

describe('Globals', function () {
  it('are defined', function () {
    expect(global).to.include.all.keys(globals)
  })
})
