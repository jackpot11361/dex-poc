/* eslint-env mocha */

import sinon from 'sinon'

import Storage from 'dom-storage'

import { expect } from 'chain-dsl/test/helpers'
import { NodejsContractLoader as ContractLoader } from '../src/NodejsContractLoader'
import * as Blotter from '../src/dex/Blotter'
import * as Order from '../src/dex/Order'
import * as Demo from '../src/dex/Demo'
import * as Dex from '../src/Dex'
import { WaitForMutation, WaitForSpecificMutation } from '../src/dex/streams/VuexMutations'
import * as TradeIntent from '../src/dex/TradeIntent'
import { config } from '../src/config.js'
import { allowance, wad2D, address, balance } from '../../chain-dsl/es6'
import * as ContractLog from '../src/dex/ContractLog'
import * as Contracts from '../src/dex/Contracts'

describe('Dex', async function () {
  this.timeout(5000)
  this.slow(1000)

  let storage, contracts, web3, whisperPollInterval

  before(async function () {
    web3 = new Web3(devChainUrl)
    const loader = ContractLoader(await web3.eth.net.getId())
    contracts = await Contracts.load(loader, web3)
    whisperPollInterval = 20
    config.whisperNodeUrl = devChainUrl
  })

  beforeEach(() => { storage = new Storage(null, {strict: true}) })

  describe('new', function () {
    it('works', () => {
      expect(() => new Dex.System({config, storage, whisperPollInterval})).to.not.throw()
    })

    it('defaults to primary dev chain', () => {
      expect(new Dex.System({config, storage, whisperPollInterval}))
        .nested.property('mmWeb3.currentProvider.host', devChainUrl)
    })
  })

  describe('#start', async () => {
    let dex

    afterEach(async () => {
      if (dex != null) {
        await dex.stop()
        dex = null
      }
    })

    function haveDemoAccounts () {
      it('has 3 named accounts', async () => {
        expect(dex.accounts.list).lengthOf(3)
        expect(dex.accounts.DEPLOYER).satisfies(Web3.utils.isAddress)
        expect(dex.accounts.ALICE).satisfies(Web3.utils.isAddress)
        expect(dex.accounts.BOB).satisfies(Web3.utils.isAddress)
      })

      it('account 0 is the selected one', async () => {
        expect(dex.accounts.selected).equals(dex.accounts.list[0])
      })
    }

    function dontHaveDemoAccounts () {
      it('has no named accounts', async () => {
        expect(dex.accounts.DEPLOYER).equal(undefined)
        expect(dex.accounts.ALICE).equal(undefined)
        expect(dex.accounts.BOB).equal(undefined)
      })

      it('has one account and that is the selected one', async () => {
        expect(dex.accounts.list).lengthOf(1)
        expect(dex.accounts.selected).equals(dex.accounts.list[0])
      })
    }

    context('without MetaMask', function () {
      beforeEach(async () => {
        dex = new Dex.System({config, ContractLoader, storage, whisperPollInterval})
        dex.contractLog = ContractLog.create({
          web3: dex.mmWeb3,
          exchange: contracts.xchg,
          netId: config.networkId,
          storage,
          pollInterval: 20
        })
        dex = await dex.start()
      })

      haveDemoAccounts()
    })

    context('with MetaMask', function () {
      context('and without the `?nomm` search param', function () {
        beforeEach(async () => {
          const fakeMetaMaskProvider = new Web3(Dex.devChainUrl).currentProvider

          dex = new Dex.System({config,
            mmProvider: fakeMetaMaskProvider,
            ContractLoader,
            storage,
            whisperPollInterval
          })
          dex.mmWeb3.eth.getAccounts = async () => ['0xALICE']
          delete dex.searchParams['nomm']

          dex.contractLog = ContractLog.create({
            web3: dex.mmWeb3,
            exchange: contracts.xchg,
            netId: config.networkId,
            storage,
            pollInterval: 20
          })

          dex = await dex.start()
        })

        dontHaveDemoAccounts()
      })

      context('and with the `?nomm` search param', function () {
        beforeEach(async () => {
          const fakeMetaMaskProvider = new Web3(Dex.devChainUrl).currentProvider

          dex = new Dex.System({config,
            mmProvider: fakeMetaMaskProvider,
            searchParams: {nomm: ''},
            ContractLoader,
            storage,
            whisperPollInterval
          })

          dex.contractLog = ContractLog.create({
            web3: dex.mmWeb3,
            exchange: contracts.xchg,
            netId: config.networkId,
            storage,
            pollInterval: 20
          })

          dex = await dex.start()
        })

        haveDemoAccounts()
      })
    })

    context('with token pair in search params', function () {
      it('selects the provided pair', async () => {
        dex = new Dex.System({config,
          ContractLoader,
          storage,
          searchParams: {pair: 'ZRX/WETH'},
          whisperPollInterval
        })

        dex.contractLog = ContractLog.create({
          web3: dex.mmWeb3,
          exchange: contracts.xchg,
          netId: config.networkId,
          storage,
          pollInterval: 20
        })

        dex = await dex.start()

        const {state: {selectedPair}} = dex.store

        expect(selectedPair).nested.property('base.symbol', 'ZRX')
        expect(selectedPair).nested.property('quote.symbol', 'WETH')
      })
    })
  })

  context('when started with mocked whisper', function () {
    let dex, ALICE, BOB, OAX, WETH

    beforeEach(async () => {
      dex = new Dex.System({
        config,
        ContractLoader,
        storage,
        whisperPollInterval
      })

      dex.contractLog = ContractLog.create({
        web3: dex.mmWeb3,
        exchange: contracts.xchg,
        netId: config.networkId,
        storage,
        pollInterval: 20
      })

      dex = await dex.start()
      ;({OAX, WETH} = dex.tokens)

      await Demo.ensure(dex.mmWeb3, dex.contracts, dex.accounts)
      ;({ALICE, BOB} = dex.accounts)
    })

    afterEach(() => { if (dex != null) { dex = dex.stop() } })

    it('the first token pair is selected', async () => {
      const {
        state: {selectedPair},
        getters: {tokenPairs: [defaultPair]}
      } = dex.store

      expect(selectedPair).eql(defaultPair)
    })

    describe('.placeOrder', function () {
      it('avoids matching taker\'s own orders')

      it('throws on selling token without allowance', async function () {
        const order = TradeIntent.create({
          address: ALICE,
          buy: {amount: D`0.000001`, token: WETH},
          sell: {amount: D`0.000001`, token: OAX}
        })

        const allowanceInWad = await allowance(
          dex.contracts.oax, ALICE, address(dex.contracts.proxy)
        )

        const aliceAllowance = wad2D(allowanceInWad)

        expect(aliceAllowance).to.eqD(D`0`)
        await expect(Dex.placeOrder(dex, order)).to.eventually.be.rejectedWith(
          'Approval required to sell `OAX` token'
        )
      })

      it('throws on selling more token than has balance for', async function () {
        const aliceBalance = wad2D(await balance(dex.contracts.weth, ALICE))

        const amountToSell = D`2`.times(aliceBalance)

        const order = TradeIntent.create({
          address: ALICE,
          intentType: 'sell',
          buy: {amount: D`0.000001`, token: OAX},
          sell: {amount: amountToSell, token: WETH}
        })

        await expect(Dex.placeOrder(dex, order)).to.eventually.be.rejectedWith(
          `Insufficient balance to sell ${amountToSell} \`WETH\` token`
        )
      })

      context('without a match', function () {
        let order, intent

        beforeEach(async () => {
          intent = TradeIntent.create({
            address: ALICE,
            intentType: 'sell',
            buy: {amount: D`0.000001`, token: OAX},
            sell: {amount: D`0.000001`, token: WETH}
          })
          order = Order.fromTradeIntent(intent)
          await Dex.placeOrder(dex, intent)
          await WaitForMutation(dex.store, ['addToOrderbook'])
        })

        it('puts order into the blotter', async function () {
          expect(dex.store.state.blotter.orders).containSubset(
            [{'data': {'order': order.orderHash}}]
          )
        })

        it.skip('broadcasts the order', async function (done) {
          // TODO How to test this in integration, without mocking?
          dex.orderTransceiver.ordersObservable.first().subscribe(done)
        })
      })

      context('with a match', function () {
        // TODO do not mix orders from two parties in one DEX

        let makerIntent, takerIntent

        beforeEach(async () => {
          makerIntent = TradeIntent.create({
            address: ALICE,
            intentType: 'buy',
            buy: {amount: D`0.000001`, token: OAX},
            sell: {amount: D`0.000001`, token: WETH}
          })
          takerIntent = TradeIntent.create({
            address: BOB,
            intentType: 'sell',
            sell: makerIntent.buy,
            buy: makerIntent.sell
          })
          await Dex.placeOrder(dex, makerIntent)
          await WaitForMutation(dex.store, ['addToOrderbook'])
        })

        it('settles', async function () {
          const trade = await Dex.placeOrder(dex, takerIntent)
          await WaitForSpecificMutation(dex.store, ['blotter/updateRecord'], commit => {
            const receivedFill = commit.mutation.payload
            return receivedFill.transactionHash === trade.fills[0].transactionHash
          })
          const record = Blotter.get(dex.blotter, trade.fills[0].transactionHash)

          expect(record.data).property('status', 'fulfilled')
          expect(record.data.filled.buy).eqD(D`0.000001`)
          expect(record.data.filled.sell).eqD(D`0.000001`)
        })
      })

      context('with a better existing order', function () {
        const amount = D`0.000004`
        const desiredAmount = amount.div('2')
        const okAmount = amount.div('4')
        let makerIntent
        let makerTrade

        beforeEach(async () => {
          makerIntent = TradeIntent.create({
            address: ALICE,
            intentType: 'buy',
            buy: {amount: amount, token: OAX},
            sell: {amount: amount, token: WETH}
          })

          makerTrade = await Dex.placeOrder(dex, makerIntent)

          await WaitForSpecificMutation(dex.store, ['addToOrderbook'], commit => {
            const receivedOrder = commit.mutation.payload
            return makerTrade.orders.some(order => {
              return order.orderHash === receivedOrder.orderHash
            })
          })
        })

        it('respects BUY intent and fills completely', async function () {
          // Having a standing offer for buy 4 sell 4.
          // Our intent is to BUY 2 for up to 4.
          // We should buy 2 for 2.

          this.timeout(10000)

          const intent = TradeIntent.create({
            intentType: 'buy',
            address: BOB,
            buy: {amount: desiredAmount, token: WETH},
            sell: {amount: amount, token: OAX}
          })
          const trade = await Dex.placeOrder(dex, intent)

          await WaitForSpecificMutation(dex.store, ['blotter/updateRecord'], commit => {
            const receivedFill = commit.mutation.payload
            return receivedFill.transactionHash === trade.fills[0].transactionHash
          })

          const record = Blotter.get(dex.blotter, trade.fills[0].transactionHash)

          expect(record.data).property('status', 'fulfilled')
          expect(record.data.filled.buy).eqD(desiredAmount)
          expect(record.data.filled.sell).eqD(desiredAmount)
        })

        it('respects SELL intent and fills completely', async function () {
          // Having a standing offer for buy 4 sell 4.
          // Our intent is to SELL 2 for at least 1.
          // We should sell 2 and buy 2.

          this.timeout(10000)

          const intent = TradeIntent.create({
            intentType: 'sell',
            address: BOB,
            sell: {amount: desiredAmount, token: OAX},
            buy: {amount: okAmount, token: WETH}
          })
          const trade = await Dex.placeOrder(dex, intent)

          await WaitForSpecificMutation(dex.store, ['blotter/updateRecord'], commit => {
            const receivedFill = commit.mutation.payload
            return receivedFill.transactionHash === trade.fills[0].transactionHash
          })

          const record = Blotter.get(dex.blotter, trade.fills[0].transactionHash)

          expect(record.data).property('status', 'fulfilled')
          expect(record.data.filled.sell).eqD(desiredAmount)
          expect(record.data.filled.buy).eqD(desiredAmount)
        })
      })
    })

    describe('blotter', function () {
      let dex2

      afterEach(() => {
        if (dex2 != null) { dex2 = dex2.stop() }
      })

      it('restores orders for the current network only', async () => {
        const intent = TradeIntent.create({
          address: ALICE,
          intentType: 'buy',
          buy: {amount: D`0.000001`, token: OAX},
          sell: {amount: D`0.000001`, token: WETH}
        })
        const {intentType, ...intentWithoutType} = intent
        await Dex.placeOrder(dex, intent)
        expect(dex.store.state.blotter.orders)
          .containSubset([{'data': intentWithoutType}])

        // When I switch networks and the browser page reloads,
        // `storage` stays the same

        dex2 = new Dex.System({config,
          chainUrl: anotherDevChainUrl,
          ContractLoader,
          storage,
          whisperPollInterval
        })
        dex2.whisperWeb3 = dex.mmWeb3
        await dex2.start()

        expect(dex2.store.state.blotter.orders).empty  // eslint-disable-line
      })
    })

    describe.skip('UI', function () {
      describe('placeOrder action', async function () {
        it('works for buy', async () => {
          const {commit, dispatch} = dex.store
          const {base, quote} = dex.store.state.selectedPair
          commit('setOrderType', 'buy')
          commit('setOrderAmount', '0.2')
          commit('setOrderPrice', '0.5')

          // FIXME hack to workaround hardcoded selected account
          dex.accounts = R.merge(dex.accounts, {selected: ALICE})

          const trade = await dispatch('placeOrder')

          expect(trade).containSubset({
            tradeIntent: {
              address: dex.accounts.selected,
              intentType: 'buy',
              buy: { amount: D`0.2`, token: base },
              sell: { amount: D`0.1`, token: quote }
            },
            fills: [],
            orders: [
              {
                address: dex.accounts.selected,
                buy: { amount: D`0.2`, token: base },
                sell: { amount: D`0.1`, token: quote },
                events: []
              }
            ]
          })
        })

        it('works for sell', async () => {
          const {commit, dispatch} = dex.store
          const {base, quote} = dex.store.state.selectedPair
          commit('setOrderType', 'sell')
          commit('setOrderAmount', '0.2')
          commit('setOrderPrice', '0.5')

          // FIXME hack to workaround hardcoded selected account
          dex.accounts = R.merge(dex.accounts, {selected: BOB})

          const trade = await dispatch('placeOrder')

          expect(trade).containSubset({
            tradeIntent: {
              address: dex.accounts.selected,
              intentType: 'sell',
              buy: { amount: D`0.1`, token: quote },
              sell: { amount: D`0.2`, token: base }
            },
            fills: [],
            orders: [
              {
                address: dex.accounts.selected,
                buy: { amount: D`0.1`, token: quote },
                sell: { amount: D`0.2`, token: base },
                events: []
              }
            ]
          })
        })
      })
    })
  })

  describe('Whisper Web3 with WebSocket', function () {
    let dex, whisperSpy

    beforeEach(async function () {
      dex = new Dex.System({config, ContractLoader, storage, whisperPollInterval})
      dex.config.whisperNodeUrl = devChainWsUrl
      dex = await dex.start()
    })

    afterEach(async function () {
      if (!R.isNil(dex)) { await dex.stop() }
      if (!R.isNil(whisperSpy)) { whisperSpy.restore() }
    })

    it('closes WebSocket on Dex#stop', async function () {
      const whisperSpy = sinon.spy(dex.whisperWeb3.currentProvider.connection, 'close')
      await dex.stop()
      expect(whisperSpy.calledOnce).equal(true)
      dex = null
    })
  })
})
