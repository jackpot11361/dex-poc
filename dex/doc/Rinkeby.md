# Rinkeby manual test procedure

* `cd ~/gitlab.com/oax/sandbox/dex`

* Start a local Rinkeby node with `bin/geth.sh rinkeby fast 3`

* If syncing the first time at the office add tamas' node as a peer (might have to update IP) for faster sync.
```
geth attach http://localhost:8420
admin.addPeer("enode://09402313762e2ff98060e0acb9c0c4974a2d158ca8f6a89ff5881d67025cc83fb9d27563a2d2cb7b104ae230afebea7adea607861963dd20554f82c2ffa5ccee@172.28.10.29:30420")
```

* Wait until it's synced. It takes about an hour for a fresh sync over local network. (Would it make sense to add this check to the deploy script?)

* Contracts can be redeployed via `bin/deploy.js http://localhost:8420 4` (takes about 10 minutes)

* `bin/deploy.js` saves the contract addresses and JSON interfaces under `net/4/{xchg,proxy,weth,oax,zrx}.json`, which are version controlled, so it's not absolutely necessary to run.
It also sets up a demo scenario, which can be reestablished independently later.

* The `oax` token contract is just a custom WETH9 instance (`contracts/OAX_WETH.sol`) with its symbol set to "OAX" and uses 18 decimal point precision

* `bin/rinkeby-demo.js` can be used to replenish test ether supplies and pump up allowances without redeploying all the contracts. It requires Rinkeby accessible via http://localhost:8420 . It requires a bit more than 6 ETH total.

* Flip `Web3(devChainUrl)` to `Web3('http://localhost:8420')` in `src/main.js`

* Start `overmind s` to open the UI

* Try a trade with 0.0002 OAX @1 for 0.0001 WETH and you should see a partial fill in the blotter and no exceptions on the console (regarding `fillOrder`, `placeOrder` or `settle`)
