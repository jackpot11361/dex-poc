// Non-third-party global state setup in ES6 format
import BigNumber from './lib/bignumber.js/bignumber.js'
import BrowserContractLoader from './BrowserContractLoader.js'
import { D } from './lib/chain-dsl/es6.js'
import * as component from './component.js'

import * as Dex from './Dex.js'
import * as metamaskUtils from './dex/metamaskUtils.js'
import './ui/filters.js'
import App from './ui/components/App.js'
import Tutorial from './ui/components/Tutorial.js'
import Market from './ui/components/Market.js'
import Landing from './ui/components/Landing.js'
import { config } from './config.js'

// Export globals
window.BigNumber = BigNumber
window.D = D
window.BN = Web3.utils.BN
window.toBN = Web3.utils.toBN
window.clone = component.clone
window.merge = component.merge
window.start = component.start
window.stop = component.stop

// Provide a more human readable representation of BigNumbers
// when they are logged via `console.log`.
BigNumber.prototype.inspect = function () {
  return 'D`' + this.toString() + '`'
}

BN.prototype.inspect = function () {
  return 'toBN(\'' + this.toString() + '\')'
}

// Hook up Chrome DevTools by enabling Custom Formatters
// in the DevTools settings (accessible via F1)
window.devtoolsFormatters = [{
  header: function (obj) {
    if (!(obj instanceof BigNumber) && !(obj instanceof BN)) {
      // Fallback to default formatting
      return null
    }
    return ['div', {}, (obj.inspect || obj.toString).bind(obj)()]
  },

  hasBody: function () {
    return false
  }
}]

// Convert obj.entries() style data into POJO
function iterator2obj (iterator) {
  const arr = Array.from(iterator)
  return Object.assign({}, ...arr.map(([k, v]) => ({[k]: v})))
}

function searchParams () {
  // URLSearchParams is not supported in IE11 & Edge 16
  // https://caniuse.com/#search=URLSearchParams
  const searchParams = new URLSearchParams(window.location.search) // eslint-disable-line no-undef
  return iterator2obj(searchParams.entries())
}

Vue.config.productionTip = false

const routes = [
  {
    path: '/',
    component: App,
    children: [
      {path: '', component: Landing, name: 'landing'},
      {path: 'tutorial', component: Tutorial, name: 'tutorial'},
      {path: 'market', component: Market, name: 'market'}
    ]
  }
]

window.boot = async () => {
  const storage = window.localStorage
  const mmProvider = window && window.web3 ? window.web3.currentProvider : null
  let dex = new Dex.System({
    config,
    chainUrl: config.chainUrl,
    mmProvider,
    storage,
    ContractLoader: BrowserContractLoader,
    searchParams: searchParams()
  })
  window.dex = dex
  window.dex = await dex.start()

  // init metamaskUtils
  metamaskUtils.init({
    store: dex.store,
    web3: dex.mmWeb3,
    mmProvider: mmProvider,
    contracts: dex.contracts
  })

  const router = new VueRouter({routes})
  //
  // router.beforeEach(function tutorialGate (to, from, next) {
  //   const skipTutorial = window.localStorage.getItem('skipTutorial') === 'yes'
  //
  //   if (to.path === '/tutorial' || skipTutorial) {
  //     next()
  //   } else {
  //     next('/tutorial')
  //   }
  // })

  const ui = new Vue({
    store: dex.store,
    router
  })
  window.ui = ui
  ui.$mount('#app')

  return {dex, ui}
}

console.debug('Booting...')
window.boot()
  .then((...args) => console.debug('main#boot() result:\n', ...args))
  .catch((...args) => console.error('main#boot() error:\n', ...args))

window.addEventListener('beforeunload', async function (e) {
  await window.dex.stop()
})
