export default {
  // language=HTML
  template: `
    <div class="marketplace panel">
      <el-select v-model="selectedPair"
                 placeholder="Select"
                 no-match-text="Token not found"
                 filterable
                 value="selectedPair"
                 value-key="key">
        <el-option
          v-for="tokenPair in tokenPairs"
          :key="tokenPair.key"
          :label="tokenPair.label"
          :value="tokenPair">
        </el-option>
      </el-select>

      <div>
        <div>Last Price</div>
        <div style="font-size: 20px; font-weight: bold">{{lastPrice? lastPrice: '--'}}</div>
      </div>
      <div style="display: flex; justify-content: space-between">
        <div style="margin-right: 50px">
          <div>24hr Change</div>
          <div style="color: rgb(28, 244, 119); font-size: 15px;">{{oneDayChange?oneDayChange:'--'}} %</div>
        </div>

        <div>
          <div>24hr Volume</div>
          <div  style="font-size: 15px">{{oneDayVolume? oneDayVolume:'--'}}</div>
        </div>

      </div>

      <div style="display: flex; justify-content: space-between">
        <div style="margin-right: 50px">
          <div>24hr High</div>
          <div  style="font-size: 15px">{{oneDayHigh? oneDayHigh: '--'}}</div>
        </div>
        <div>
          <div>24hr Low</div>
          <div  style="font-size: 15px">{{oneDayLow?oneDayLow:'--'}}</div>
        </div>

      </div>
    </div>
  `,

  data: () => {
    return {
      activeTab: 'limit-order',
      lastPrice: null,
      oneDayChange: null,
      oneDayHigh: null,
      oneDayLow: null,
      oneDayVolume: null
    }
  },

  methods: {
    changeTabTo (tab) { this.activeTab = tab },

    ...Vuex.mapMutations([
      'selectPair'
    ]),

    ...Vuex.mapMutations('ui', [
      'setOrderAmount',
      'setOrderPrice',
      'setOrderType'
    ]),

    ...Vuex.mapActions('ui', [
      'placeOrder'
    ])
  },

  computed: {
    selectedPair: {
      get () { return this.$store.state.selectedPair },
      set (pair) { this.$store.commit('selectPair', pair) }
    },

    action () {
      return this.$store.state.ui.orderType
    },

    limitOrderConfirmClasses () {
      return (this.action === 'buy')
        ? 'btn--confirm-buy'
        : 'btn--confirm-sell'
    },

    limitOrderConfirmLabel () {
      return (this.action === 'buy')
        ? 'Place Buy Order'
        : 'Place Sell Order'
    },

    ...Vuex.mapGetters([
      'tokenPairs'
    ]),
    ...Vuex.mapGetters('tradeHistory', [
      'getOneDayHistory'
    ])
  },

  watch: {

    selectedPair: function (pair) {
      console.debug('switching to pair', pair.label)
      const params = new URLSearchParams(window.location.search) // eslint-disable-line no-undef
      params.set('pair', pair.label)
      window.location = `${window.location.pathname}?${params}#/market`
    },

    getOneDayHistory: function (newHistory, oldHistory) {
      let lastValue = R.last(newHistory)
      let secondLastValue = newHistory.length > 1 ? newHistory[newHistory.length - 2] : lastValue

      this.lastPrice = lastValue.ophl[1]
      this.oneDayChange = D(lastValue.ophl[1]).minus(D(secondLastValue.ophl[1])).dividedBy(D(secondLastValue.ophl[1])).times(100).toFixed(2)
      this.oneDayHigh = lastValue.ophl[3]
      this.oneDayLow = lastValue.ophl[2]

      this.oneDayVolume = lastValue.volumn.toFixed(8)
    }
  }
}
