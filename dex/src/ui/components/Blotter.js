export default {
  // language=HTML
  template: `
        <div class="panel panel--blotter">
        <!--<h3>BLOTTER</h3>-->

          <nav class="tab-bar" style="margin-bottom: 20px">
            <a class="tab-bar__tab" href="#two"
               @click.prevent="changeTabTo('open-order')"
               :class="{'is-active': activeTab === 'open-order'}">Open Order</a>

            <a class="tab-bar__tab"
               @click.prevent="changeTabTo('filled-order')"
               :class="{'is-active': activeTab === 'filled-order'}">
              Filled Order
            </a>
            <a class="tab-bar__tab"
               @click.prevent="changeTabTo('order-history')"
               :class="{'is-active': activeTab === 'order-history'}">
              Order History
            </a>
          </nav>

          <div v-if="activeTab === 'open-order'" class="tab-content">

            <table class="table">
              <thead class="table__header">
              <tr>
                <th>PAIR</th>
                <th>TYPE</th>
                <th>AMOUNT</th>
                <th>FILLED</th>
                <th>PRICE</th>
                <th>EXPIRES</th>
                <th>STATUS</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody class="table__body">
              <tr style="height: 48px; background-color: black; margin-bottom: 4px;" v-for="record of openOrders" data-order-type="buy">
                <td>{{record.data.price.tokenPair.label}}</td>
                <td v-bind:style="{ 'color': orderType(record.data) === 'Buy' ? '#1CF477': '#FF5800' }">{{orderType(record.data)}}</td>
                <td>{{orderAmount(record.data).toFixed(8)}}</td>
                <td>{{filledAmount(record.data).toFixed(8)}}</td>
                <td>{{record.data.price.amount.toFixed(8)}}</td>
                <td>{{expiryDateOfOrder(record.data)}}</td>
                <td>{{record.data.status}}</td>
                <th>
                  <a @click="cancelOrder(record.data)" style="cursor:pointer; color: #FF0058; border: 1px solid #FF0058; width: 45px; height: 20px; padding: 4px;">
                    Cancel
                  </a>
                </th>
              </tr>
              </tbody>
            </table>

          </div>
          <div v-if="activeTab === 'filled-order'" class="tab-content">

            <table class="table">
              <thead class="table__header">
              <tr>
                <th>PAIR</th>
                <th>TYPE</th>
                <th>AMOUNT</th>
                <th>FILLED</th>
                <th>PRICE</th>
                <th>EXPIRES</th>
                <th>STATUS</th>
              </tr>
              </thead>
              <tbody class="table__body">
              <tr style="height: 48px; background-color: black; margin-bottom: 4px;" v-for="record of filledOrders" data-order-type="buy">
                <td>{{record.data.price.tokenPair.label}}</td>
                <td v-bind:style="{ 'color': orderType(record.data) === 'Buy' ? '#1CF477': '#FF5800' }">{{orderType(record.data)}}</td>
                <td>{{orderAmount(record.data).toFixed(8)}}</td>
                <td>{{filledAmount(record.data).toFixed(8)}}</td>
                <td>{{record.data.price.amount.toFixed(8)}}</td>
                <td>{{expiryDateOfOrder(record.data)}}</td>
                <td>{{record.data.status}}</td>
              </tr>
              </tbody>
            </table>

          </div>

          <div v-if="activeTab === 'order-history'" class="tab-content" >

            <table class="table">
              <thead class="table__header">
              <tr>
                <th>PAIR</th>
                <th>TYPE</th>
                <th>AMOUNT</th>
                <th>FILLED</th>
                <th>PRICE</th>
                <th>EXPIRES</th>
                <th>STATUS</th>
              </tr>
              </thead>
              <tbody class="table__body">
              <tr style="height: 48px; background-color: black; margin-bottom: 4px;" v-for="record of otherOrders" data-order-type="buy">
                <td>{{record.data.pair}}</td>
                <td v-bind:style="{ 'color': orderType(record.data) === 'Buy' ? '#1CF477': '#FF5800' }">{{orderType(record.data)}}</td>
                <td>{{orderAmount(record.data).toFixed(8)}}</td>
                <td>{{filledAmount(record.data).toFixed(8)}}</td>
                <td>{{record.data.price.amount.toFixed(8)}}</td>
                <td>{{expiryDateOfOrder(record.data)}}</td>
                <td>{{record.data.status}}</td>
              </tr>
              </tbody>
            </table>

          </div>

    </div>`,

  data: () => {
    return {
      activeTab: 'open-order'
    }
  },
  computed: {
    ...Vuex.mapGetters('blotter', [
      'getRecordsSortedByAge'
    ]),
    selectedAccount () {
      return this.$store.getters['accounts']().selected
    },
    selectedPair: {
      get () { return this.$store.state.selectedPair }
    },
    openOrders: {
      get () {
        return this.getRecordsSortedByAge(this.selectedAccount, this.selectedPair)
          .filter(o => ['partiallyFilled', 'active', 'pendingConfirmation'].includes(o.data.status))
      }
    },
    filledOrders: {
      get () {
        return this.getRecordsSortedByAge(this.selectedAccount, this.selectedPair)
          .filter(o => o.data.status === 'fulfilled')
      }
    },
    otherOrders: {
      get () {
        return this.getRecordsSortedByAge(this.selectedAccount, this.selectedPair)
          .filter(o => ['expired', 'error', 'cancelled'].includes(o.data.status))
      }
    }
  },

  methods: {
    changeTabTo (tab) { this.activeTab = tab },
    expiryDateOfOrder (order) {
      const d = new Date(order.exp * 1000)
      return `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`
    },

    orderType (order) {
      return (order.buy.token.address === this.selectedPair.base.address ? 'Buy' : 'Sell')
    },

    orderAmount (record) {
      const isBuyingBase = (record.buy.token.address === this.selectedPair.base.address)
      return isBuyingBase ? record.buy.amount : record.sell.amount
    },

    filledAmount (record) {
      const isBuyingBase = (record.buy.token.address === this.selectedPair.base.address)
      return isBuyingBase ? record.filled.buy : record.filled.sell
    },
    ...Vuex.mapActions([
      'cancelOrder'
    ])
  }
}
