import Account from './Account.js'

export default {
  name: 'Navbar',

  template: `
        <div class="navbar">
        <div class="navbar--branding">
        <a href="#/">
            <img src="images/logo.svg" width="96" height="32" >
        </a>
        
         <router-link to="market">Prototype</router-link>
         <router-link to="tutorial">Quick Start</router-link>
        
        </div>
        <el-popover  ref="popover1" placement="top-start" trigger="click" >
            
            <div v-if="getCurrentAccount !== null" >
                <account/>
                <!--<br/>-->
                <!--<div>-->
                    <!--<router-link to="/account">Account Overview</router-link>-->
                <!--</div>-->
                <!--<br/>-->
                <!--<div>-->
                   <!--<router-link to="/wrap">Wrap or Unwrap WETH</router-link>-->
                <!--</div>-->
                <br/>
            </div>
            
            <div class="navvar--popper-box"  v-if="getSignInError !== null && getSignInError==='NO_METAMASK'" style="text-align: center;">
                    <div>
                         <a href="https://metamask.io/">
                            <img src="images/metamask.png">
                         </a>
                    </div>
                    <p>We didn’t detect Metamask. </p>
                    <p>Please install it to continue using OAX dex.</p>
                   
            </div>
            
            
            <div  v-if="getSignInError !== null && getSignInError==='METAMASK_LOCKED'"  style="text-align: center;">
                    <p>Metamask ACCOUNT LOCKED</p>
                    <p>Unlock your Metamask wallet to continue using OAX dex.</p>
            </div>
            
              <div  v-if="getSignInError !== null && getSignInError==='WRONG_NETWORK'"  style="text-align: center;">
                    <p>Connecting to wrong network</p>
                    <p>Please connect your Metamask to {{requiredNetwork.name}} </p>
            </div>
            <div  v-if="version" class="landing--version">Revision: {{version}}</div>
        </el-popover>
        <div class="navbar--end" v-popover:popover1  >
            <div style="width: 15px; margin-right: 10px"><img src="images/user.svg"></div>
            <div  v-if="getCurrentAccount !== null" >
                <div class="navbar-item">
                  <div class="field is-grouped">
                    <p class="control">
                      {{getCurrentAccount}}
                    </p>
                  </div>
                </div>
            </div>
            <div v-if="getSignInError !== null">
                <div class="navbar-item">
                  <div class="field is-grouped">
                    <p class="control navbar--error" v-if="getSignInError==='NO_METAMASK'">METAMASK NOT INSTALLED</p>
                    <p class="control navbar--error" v-if="getSignInError==='METAMASK_LOCKED'">METAMASK LOCKED</p>
                    <p class="control navbar--error" v-if="getSignInError==='WRONG_NETWORK'">WRONG NETWORK</p>
                  </div>
                </div>
            </div>
        </div>
        </div>
    `,

  data: function () {
    return {
      version: null
    }
  },
  computed: Vuex.mapGetters('metamaskUtils', [
    'getCurrentAccount', 'getSignInError', 'requiredNetwork'
  ]),

  components: {
    Account
  },

  async created () {
    this.$store.dispatch('metamaskUtils/start')
  },

  async mounted () {
    if (window.innerWidth < 480) {
      const h = this.$createElement
      this.$notify({
        message: h('div', null, 'Not supported device, please use desktop version'),
        duration: 0
      })
    }

    try {
      const res = await window.fetch('./version')
      if (res.ok) this.version = await res.text()
    } catch (err) {
      console.debug('Failed to get deployment commit hash. Rev label disabled.')
    }

    if (this.version) {
      const checkVersionHandler = setInterval(async () => {
        try {
          const res = await window.fetch('./version')
          if (res.ok) {
            const fetchedVersion = await res.text()

            if (fetchedVersion !== this.version) {
              this.$notify({
                title: 'New Update Available',
                message: 'A new release is now available. Reload to update.',
                duration: 0
              })

              clearInterval(checkVersionHandler)
            }
          }
        } catch (err) {
          console.debug(err)
        }
      }, 1000 * 60 * 15)
    }
  }
}
