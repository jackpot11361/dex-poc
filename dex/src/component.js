export function clone (obj) {
  return merge(obj, {})
}

export function merge (obj, ...extensions) {
  let sameKindButEmptyObj = Object.create(Object.getPrototypeOf(obj))

  // `Object.setPrototype` might cause cascading performance degradation
  // according to https://developer.mozilla.org/en-US/docs/Web/JavaScript
  //    /Reference/Global_Objects/Object/setPrototypeOf
  return Object.assign(sameKindButEmptyObj, R.mergeAll([obj, ...extensions]))
}

/*
(ns com.stuartsierra.component
  (:require [com.stuartsierra.dependency :as dep]
            [com.stuartsierra.component.platform :as platform]))

(defprotocol Lifecycle
  (start [component]
    "Begins operation of this component. Synchronous, does not return
  until the component is started. Returns an updated version of this
  component.")
  (stop [component]
    "Ceases operation of this component. Synchronous, does not return
  until the component is stopped. Returns an updated version of this
  component."))
*/
export class Lifecycle {
  async start () {
    return this
  }

  async stop () {
    return this
  }
}

/*
;; No-op implementation if one is not defined.
(extend-protocol Lifecycle
  #?(:clj java.lang.Object :cljs default)
  (start [this]
    this)
  (stop [this]
    this))
*/

// Instead of polluting the global `Object.prototype` with start/stop methods,
// provide start/stop helper functions
export async function start (component) {
  if (R.is(Object, component) && ('start' in component)) {
    return component.start()
  } else {
    return component
  }
}

export async function stop (component) {
  if (R.is(Object, component) && ('stop' in component)) {
    return component.stop()
  } else {
    return component
  }
}

// For turning an existing class into a Component,
// we can use this abstract mixin class.
// Docs: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes#Mix-ins
export function mixinLifecycle (BaseClass) {
  return class extends BaseClass {
    async start () {
      return this
    }

    async stop () {
      return this
    }
  }
}

/*
(defn dependencies
  "Returns the map of other components on which this component depends."
  [component]
  (::dependencies (meta component) {}))
*/
export function dependencies (component) {
  if ('DEPS' in component) {
    return component.DEPS
  } else {
    return {}
  }
}

/*
(defn using
  "Associates metadata with component describing the other components
  on which it depends. Component dependencies are specified as a map.
  Keys in the map correspond to keys in this component which must be
  provided by its containing system. Values in the map are the keys in
  the system at which those components may be found. Alternatively, if
  the keys are the same in both the component and its enclosing
  system, they may be specified as a vector of keys."
  [component dependencies]
  (vary-meta
   component update-in [::dependencies] (fnil merge {})
   (cond
    (map? dependencies)
      dependencies
    (vector? dependencies)
      (into {} (map (fn [x] [x x]) dependencies))
    :else
      (throw (ex-info "Dependencies must be a map or vector"
                      {:reason ::invalid-dependencies
                       :component component
                       :dependencies dependencies})))))
*/
export function using (component, dependencies) {
  return merge(component, {DEPS: dependencies})
}

/*
(defn- nil-component [system key]
  (ex-info (str "Component " key " was nil in system; maybe it returned nil from start or stop")
           {:reason ::nil-component
            :system-key key
            :system system}))
*/
function nilComponent (system, key) {
  return Error(`Component ${key} was nil in system;` +
        ` maybe it returned nil from start or stop`)
}

/*
(defn- get-dependency [system system-key component dependency-key]
  (let [dependency (get system system-key ::not-found)]
    (when (nil? dependency)
      (throw (nil-component system system-key)))
    (when (= ::not-found dependency)
      (throw (ex-info (str "Missing dependency " dependency-key
                           " of " (platform/type-name component)
                           " expected in system at " system-key)
                      {:reason ::missing-dependency
                       :system-key system-key
                       :dependency-key dependency-key
                       :component component
                       :system system})))
    dependency))
*/
function getDependency (system, systemKey, component, dependencyKey) {
  const NOT_FOUND = 'NOT_FOUND'
  const dependency = R.propOr(NOT_FOUND, systemKey, system)
  if (dependency === null) throw nilComponent(system, systemKey)
  if (dependency === NOT_FOUND) {
    throw Error('Missing dependency ' + dependencyKey +
            ' of ' + typeof component + ' expected in system at ' + systemKey)
  }
  return dependency
}

/*
(defn system-using
  "Associates dependency metadata with multiple components in the
  system. dependency-map is a map of keys in the system to maps or
  vectors specifying the dependencies of the component at that key in
  the system, as per 'using'."
  [system dependency-map]
  (reduce-kv
   (fn [system key dependencies]
     (let [component (get-component system key)]
       (assoc system key (using component dependencies))))
   system
   dependency-map))
*/
export function systemUsing (system, dependencyMap) {
  throw Error('Not implemented')
}

/*
(defn- assoc-dependency [system component dependency-key system-key]
  (let [dependency (get-dependency system system-key component dependency-key)]
    (assoc component dependency-key dependency)))
*/
export function assocDependency (system, component, dependencyKey, systemKey) {
  const dependency = getDependency(system, systemKey, component,
    dependencyKey)
  return merge(component, {[dependencyKey]: dependency})
}

/*
(defn- assoc-dependencies [component system]
  (reduce-kv #(assoc-dependency system %1 %2 %3)
             component
             (dependencies component)))
*/
export function assocDependencies (component, system) {
  return R.reduce((acc, [k, v]) => assocDependency(system, acc, k, v),
    component,
    Object.entries(dependencies(component)))
}

/*
(defn- try-action [component system key f args]
  (try (apply f component args)
       (catch #?(:clj Throwable :cljs :default) t
         (throw (ex-info (str "Error in component " key
                              " in system " (platform/type-name system)
                              " calling " f)
                         {:reason ::component-function-threw-exception
                          :function f
                          :system-key key
                          :component component
                          :system system}
                         t)))))
*/
async function tryAction (component, system, key, f, args) { // eslint-disable-line no-unused-vars
  throw Error('Not implemented')
}

/*
(defn update-system
  "Invokes (apply f component args) on each of the components at
  component-keys in the system, in dependency order. Before invoking
  f, assoc's updated dependencies of the component."
  [system component-keys f & args]
  (let [graph (dependency-graph system component-keys)]
    (reduce (fn [system key]
              (assoc system key
                     (-> (get-component system key)
                         (assoc-dependencies system)
                         (try-action system key f args))))
            system
            (sort (dep/topo-comparator graph) component-keys))))
*/
export async function updateSystem (system, componentKeys, f, ...args) {
  throw Error('Not implemented; needs a JS graph lib')
}

/*
(defn update-system-reverse
  "Like update-system but operates in reverse dependency order."
  [system component-keys f & args]
  (let [graph (dependency-graph system component-keys)]
    (reduce (fn [system key]
              (assoc system key
                     (-> (get-component system key)
                         (assoc-dependencies system)
                         (try-action system key f args))))
            system
            (reverse (sort (dep/topo-comparator graph) component-keys)))))
*/
export async function updateSystemReverse (system, componentKeys, f, ...args) {
  throw Error('Not implemented')
}

/*
(defn start-system
  "Recursively starts components in the system, in dependency order,
  assoc'ing in their dependencies along the way. component-keys is a
  collection of keys (order doesn't matter) in the system specifying
  the components to start, defaults to all keys in the system."
  ([system]
     (start-system system (keys system)))
  ([system component-keys]
     (update-system system component-keys #'start)))
*/
export async function startSystem (system, componentKeys) {
  throw Error('Not implemented')
}

/*
(defn stop-system
  "Recursively stops components in the system, in reverse dependency
  order. component-keys is a collection of keys (order doesn't matter)
  in the system specifying the components to stop, defaults to all
  keys in the system."
  ([system]
     (stop-system system (keys system)))
  ([system component-keys]
     (update-system-reverse system component-keys #'stop)))
*/

export async function stopSystem (system, componentKeys) {
  throw Error('Not implemented')
}

/*
(defrecord SystemMap []
  Lifecycle
  (start [system]
    (start-system system))
  (stop [system]
    (stop-system system)))
*/
/* `System` can implement automatic component
 * start/stop in dependency order, BUT for now
 * they should be overwritten by specific systems
 * which have to start and inject the components
 * in the right order
 * */

export class SystemMap extends Lifecycle {
  constructor (opts = {}) {
    super()
    Object.assign(this, opts)
  }

  async start () {
    throw Error('System#start() is not implemented')
  }

  async stop () {
    throw Error('System#stop() is not implemented')
  }
}
