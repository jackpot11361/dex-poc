export default function BrowserContractLoader (netId) {
  return async (contractName) => {
    const jsonInterfaceUrl = [
      'net',
      netId.toString(),
      `${contractName}.json`].join('/')
    const response = await fetch(jsonInterfaceUrl) // eslint-disable-line no-undef
    if (response.ok) {
      return response.json()
    } else {
      throw Error(
        `Contract JSON interface doesn't exist: ${jsonInterfaceUrl}`)
    }
  }
}
