import {
  Lifecycle,
  assocDependencies,
  SystemMap,
  using
} from './component'
import {ContractsComponent} from './ContractsComponent'
import { NodejsContractLoader } from '../src/NodejsContractLoader'

// export class Web3Component extends mixinLifecycle(Web3) {
//     async start() {
//         return this
//     }
//
//     async stop() {
//         return this
//     }
// }

export class AccountsComponent extends Lifecycle {
  constructor ({accountNames = [], /* DEPS */ web3} = {}) {
    super()
    Object.assign(this, {accountNames, web3})
  }

  async start () {
    const [DEPLOYER, ALICE, BOB] = await this.web3.eth.getAccounts()
    return merge(this, {DEPLOYER, ALICE, BOB})
  }

  async stop () {
    this.accountNames.forEach(acc => delete this[acc])
    return this
  }
}

export class TestDexSystem extends SystemMap {
  constructor (opts = {}) {
    super()
    const {
      chainUrl = devChainUrl,
      expectedNetworkId = 1337
    } = opts

    Object.assign(this, {
      expectedNetworkId,
      // web3: new Web3Component({chainUrl, expectedNetworkId}),

      web3: new Web3(chainUrl),

      accounts: using(new AccountsComponent({
        accountNames: ['DEPLOYER', 'ALICE', 'BOB']
      }), {web3: 'web3'}),

      contracts: using(new ContractsComponent({
        Loader: NodejsContractLoader,
        contractNames: ['xchg', 'proxy', 'weth', 'oax']
      }), {web3: 'web3'})
    })
  }

  async start () {
    let sys = this

    // TODO Move the following into web3.start(), to allow:
    //   sys = merge(sys, {web3: await start(assocDependencies(sys.web3, sys))})
    const web3 = sys.web3
    // await waitForConnection(web3) // TODO it's in chain-dsl atm
    const networkId = await web3.eth.net.getId()
    if (networkId !== this.expectedNetworkId) {
      throw Error(`Expected to connect to Ethereum network ${this.expectedNetworkId},` +
                ` but connected to ${networkId} instead`)
    }
    sys = merge(sys, {web3})
    sys = merge(sys,
      {accounts: await start(assocDependencies(sys.accounts, sys))})
    sys = merge(sys,
      {contracts: await start(assocDependencies(sys.contracts, sys))})

    // // FIXME This should work with WebSocket base web3 too:
    // // const deployedContracts = await Contracts.load(loader, web3)
    // // ;({contracts} = await Demo.ensure(web3, deployedContracts, {DEPLOYER, ALICE, BOB}))
    //
    // contracts = await Contracts.load(loader, web3)
    // const httpWeb3 = new Web3(devChainUrl)
    // await Demo.ensure(httpWeb3, await Contracts.load(loader, httpWeb3), {DEPLOYER, ALICE, BOB})

    return this
  }

  async stop () {
    return this
  }
}
