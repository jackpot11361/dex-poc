export class CancelIntent {
  constructor ({order, cancelAmt}) {
    this.order = order
    this.cancelAmt = cancelAmt

    Object.freeze(this)
  }
}

export function create (params) {
  return new CancelIntent(params)
}
