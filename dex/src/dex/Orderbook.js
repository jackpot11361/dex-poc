import { ceil } from '../lib/chain-dsl/es6.js'
import * as Order from './Order.js'
import * as OrderStorage from './OrderStorage.js'
import { makeChannel, mapMutation } from './utils.js'
import { OrderExpiry } from './streams/OrderExpiry.js'
import { VuexMutationsPayload } from './streams/VuexMutationsPayload.js'
import { isOrderMessage, toRFOBMessage, isRFOBMessage, toOrderMessage } from './messaging/Message.js'

/// /////////////////////////////////////
// ORDERBOOK STATE
/// /////////////////////////////////////

const moduleDefinition = {
  state () {
    return {
      orders: [],
      fillableMakerTokenAmounts: new Map()
    }
  },
  getters: {
    sortedBuyOrders,
    sortedSellOrders,
    aggregatedBuyOrders,
    aggregatedSellOrders
  },
  mutations: {
    addToOrderbook,
    removeFromOrderbook,
    updateInOrderbook
  }
}

/// /////////////////////////////////////
// ORDERBOOK INTERFACE
/// /////////////////////////////////////

const moduleName = 'orderbook'

export function create ({
  store,
  storage,
  netId,
  sendMessageChannel,
  receiveMessageChannel,
  tokenPair,
  web3,
  contracts
}) {
  store.registerModule(moduleName, moduleDefinition)

  const orderStorage = OrderStorage.create({
    store,
    storage,
    keyPrefix: moduleName,
    addOrder: 'addToOrderbook',
    saveOn: [
      'addToOrderbook',
      'updateInOrderbook',
      'removeFromOrderbook'],
    statePath: [moduleName, 'orders']
  })

  const orderExpirySource = VuexMutationsPayload(store, ['addToOrderbook'])

  const orderExpiry = OrderExpiry(orderExpirySource)

  return {
    store,
    storage,
    netId,
    moduleState: store.state[moduleName],
    orderStorage,
    orderExpirySource,
    orderExpiry,
    sendMessageChannel,
    receiveMessageChannel,
    tokenPair,
    web3,
    contracts,
    subscriptions: [],
    events: makeChannel()
  }
}

export function start (orderbook) {
  let {
    store,
    orderExpiry,
    orderStorage,
    netId,
    subscriptions
  } = orderbook

  const orderExpirySubscription = orderExpiry.subscribe(order => {
    console.debug('[Orderbook] Removing expired order', order)
    store.commit('removeFromOrderbook', order)
  })

  subscriptions.push(orderExpirySubscription)
  requestActiveOrders(orderbook)
  handleIncomingOrders(orderbook)

  orderStorage = {...orderStorage, netId}
  orderStorage = orderStorage.init()

  handleRFOBMessages(orderbook)

  return {...orderbook, orderStorage, orderExpirySubscription}
}

export function stop (orderbook) {
  let {orderStorage, subscriptions} = orderbook
  orderStorage = orderStorage.destroy()
  subscriptions.forEach(s => s.unsubscribe())
  // orderbook.store.unregisterModule(orderbook.moduleName)

  return {...orderbook, orderStorage, orderExpirySubscription: () => {}}
}

export function orders (orderbook) {
  return orderbook.moduleState.orders
}

export function size (orderbook) {
  return orderbook.moduleState.orders.length
}

export function get (orderbook, orderHash) {
  return orderbook.moduleState.orders.find(o => o.orderHash === orderHash)
}

export function contains (orderbook, order) {
  const state = orderbook.moduleState
  return state.orders.some(o => o.orderHash === order.orderHash)
}

export function requestActiveOrders (orderbook) {
  console.debug('[Orderbook] Requesting for orderbook')
  orderbook.sendMessageChannel.next(
    toRFOBMessage(orderbook.tokenPair)
  )
}

export const insert = mapMutation('addToOrderbook')
export const remove = mapMutation('removeFromOrderbook')
export const update = mapMutation('updateInOrderbook')

/// /////////////////////////////////////
// VUEX GETTERS
/// /////////////////////////////////////

// "Buy order" refers to the orders where buy token is base token
// sorting from highest price to lowest price
function sortedBuyOrders (state, getters, rootState) {
  const tokenPair = rootState.selectedPair
  const {base, quote} = tokenPair
  const orders = state.orders
    .filter(o => o.buy.token.address === base.address)
    .filter(o => o.sell.token.address === quote.address)
    .map(o => Order.withPrice(o, tokenPair))

  return orders.sort((a, b) => b.price.amount.minus(a.price.amount).toNumber())
}

// "Sell order" refers to the orders where sell token is base token
// sorting from highest price to lowest price
function sortedSellOrders (state, getters, rootState) {
  const tokenPair = rootState.selectedPair
  const {base, quote} = tokenPair
  const orders = state.orders
    .filter(o => o.sell.token.address === base.address)
    .filter(o => o.buy.token.address === quote.address)
    .map(o => Order.withPrice(o, tokenPair))

  return orders.sort((a, b) => b.price.amount.minus(a.price.amount).toNumber())
}

function aggregatedBuyOrders (state, getters, rootState) {
  const orders = sortedBuyOrders(state, getters, rootState)
  return aggregateOrderByPrice(orders)
}

function aggregatedSellOrders (state, getters, rootState) {
  const orders = sortedSellOrders(state, getters, rootState)
  return aggregateOrderByPrice(orders)
}

export function aggregateOrderByPrice (orders) {
  const roundedOrders = orders.map(order => {
    return merge(order, {
      price: {
        amount: ceil(order.price.amount, 8),
        tokenPair: order.price.tokenPair
      }
    })
  })

  const groupedOrders = R.groupWith(
    (o1, o2) => ceil(o1.price.amount, 8) === o2.price.amount, roundedOrders)
    .map(groupedOrders => {
      return groupedOrders.reduce((orders, order) => {
        orders.price = {amount: order.price.amount}
        orders.buy.amount = orders.buy.amount.plus(order.buy.amount)
        orders.sell.amount = orders.sell.amount.plus(order.sell.amount)
        orders.buy.token = order.buy.token
        orders.sell.token = order.sell.token

        return orders
      }, {
        buy: {amount: D`0`},
        sell: {amount: D`0`}
      })
    })

  return groupedOrders
}

/// /////////////////////////////////////
// VUEX MUTATIONS
/// /////////////////////////////////////

function addToOrderbook (state, order) {
  if (!Order.isNotExpired(order)) {
    throw Error('[addToOrderbook] Order expired')
  }
  if (state.orders.some(o => o.orderHash === order.orderHash)) {
    throw Error('[addToOrderbook] Duplicated order')
  }
  if (!order.orderHash || !order.orderHash.match(/0x/i)) {
    throw Error('[addToOrderbook] Order hash missing')
  }

  state.orders.push(order)
}

function removeFromOrderbook (state, order) {
  console.debug('[Orderbook] Removing order', order)
  state.orders = state.orders.filter(o => o.orderHash !== order.orderHash)
}

function updateInOrderbook (state, order) {
  const ob = state.orders
  const orderIndex = R.findIndex(R.propEq('orderHash', order.orderHash))(ob)

  if (orderIndex === -1) return false

  state.orders = R.update(orderIndex, order, ob)

  return state.orders
}

function handleIncomingOrders (orderbook) {
  const subscription = orderbook.receiveMessageChannel
    .filter(isOrderMessage)
    .subscribe(
      async message => {
        let order = message.data
        let error
        try {
          const receivedOrder = Order.WhisperOrder(message.data)
          const restoredOrder = Order.SignedOrderValidator(receivedOrder)
          await validateOrderSignature(orderbook, restoredOrder)
          validateContractAddresses(orderbook.contracts, restoredOrder)
          const validOrder = Order.create(restoredOrder)
          orderbook.store.commit('addToOrderbook', validOrder)
        } catch (err) {
          console.debug('[Orderbook.handleIncomingOrders]', err)
          error = err
        }

        orderbook.events.next({
          order,
          error
        })
      }
    )

  orderbook.subscriptions.push(subscription)
}

async function validateOrderSignature (orderbook, order) {
  const orderHash = Order.hash(orderbook.contracts, order)
  const makerAddress = await orderbook.web3.eth.personal.ecRecover(
    orderHash,
    order.sig
  )

  if (makerAddress.toLowerCase() !== order.address.toLowerCase()) {
    throw Error('Order message has invalid signature')
  }
}

function validateContractAddresses (contracts, order) {
  if (R.isNil(Order.findContract(contracts, order.buy.token.address)) ||
      R.isNil(Order.findContract(contracts, order.sell.token.address))) {
    throw Error('Order contains unlisted token address.')
  }
}

function handleRFOBMessages (orderbook) {
  const subscription = orderbook.receiveMessageChannel
    .filter(isRFOBMessage)
    .subscribe(message => {
      orders(orderbook)
        .filter(order => order.status === 'active' || order.status === 'partiallyFilled')
        .forEach(order =>
          orderbook.sendMessageChannel.next(
            toOrderMessage(Order.WhisperOrder(order))
          )
        )
    })

  orderbook.subscriptions.push(subscription)
}
