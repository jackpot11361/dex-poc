import * as Order from './Order.js'

export function create (orderStorageOpts) {
  return {
    ...orderStorageOpts,

    unsubscribe () {},

    init () {
      const {store, storage, keyPrefix, addOrder, saveOn, statePath, netId} = this
      const key = `${keyPrefix}-${netId}`
      this.unsubscribe = store.subscribe((mutation, state) => {
        if (saveOn.includes(mutation.type)) {
          save({key, storage, state, statePath})
        }
      })

      restore({key, addOrder, saveOn, store, storage, netId})
      return {...this, key}
    },

    destroy () { this.unsubscribe() }
  }
}

export function load ({key, storage}) {
  const orders = JSON.parse(storage.getItem(key))

  if (!Array.isArray(orders)) {
    return []
  } else {
    return orders.map(o => Order.create(R.omit(['price'], o)))
  }
}

function restore (orderStorage) {
  const {key, addOrder, /* saveOn, */ store, storage} = orderStorage
  load({key, storage}).forEach(order => {
    try {
      store.commit(addOrder, order)
    } catch (err) {
      console.error(err)
    }
  })
}

function save ({key, state, storage, statePath}) {
  storage.setItem(key, JSON.stringify(R.path(statePath, state)))
}
