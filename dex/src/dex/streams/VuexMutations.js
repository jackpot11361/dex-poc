export function VuexMutations (store, mutations) {
  const observable = Rx.Observable.create(observer => {
    const unsubscribe = store.subscribe((mutation, state) => {
      if (mutations.includes(mutation.type)) {
        observer.next({mutation, state})
      }
    })

    return unsubscribe
  })

  return observable
}

export function WaitForMutation (store, mutations) {
  return new Promise(resolve => {
    VuexMutations(store, mutations).first().subscribe(resolve)
  })
}

export function WaitForSpecificMutation (store, mutations, matcher) {
  return new Promise(resolve => {
    const subscription = VuexMutations(store, mutations).subscribe(
      async commit => {
        if (await matcher(commit)) {
          subscription.unsubscribe()
          resolve(commit)
        }
      }
    )
  })
}
