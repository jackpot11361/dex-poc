// eslint-disable-next-line no-undef
onmessage = function (e) {
  const [host, filter] = e.data

  let rpcRequestObject = {
    body: JSON.stringify({
      jsonrpc: '2.0',
      method: 'shh_deleteMessageFilter',
      params: [filter],
      id: Math.trunc(Math.random() * 1e16)
    }),
    headers: {
      'content-type': 'application/json'
    },
    method: 'POST'
  }

  // eslint-disable-next-line no-undef
  fetch(host, rpcRequestObject).then(close)
}
