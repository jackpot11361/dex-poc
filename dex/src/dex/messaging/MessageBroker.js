import { Message } from './Message.js'
import { tokenPairToHash, struct, isWeb3ProviderSubscriptionCapable } from '../utils.js'

const { utils: { hexToUtf8, utf8ToHex } } = Web3

export const TOPICS = {
  // First 10 characters of Web3.utils.sha3('OAX DEX')
  OAX_ORDERS: '0xf63d04b2'
}

const MessageBroker = struct.interface(
  {
    web3: 'object',
    pollInterval: 'number',
    powRetry: 'number',
    tokenPair: 'object',
    sendChannel: 'object',
    receiveChannel: 'object'
  },
  // DEFAULTS
  {
    pollInterval: 1000,
    powRetry: 3
  }
)

const WhisperEnvelope = struct(
  {
    payload: 'string',
    ttl: 'number',
    topic: 'hex',
    powTarget: 'number',
    powTime: 'number',
    symKeyID: 'hex'
  },
  // DEFAULT
  {
    ttl: 70,
    powTime: 10
  }
)

export function create (messageBrokerOptions) {
  const broker = MessageBroker(messageBrokerOptions)

  broker.symKeyID = null
  broker.topicFilters = []
  broker.subscriptions = []
  broker.sendMessageConfirmation = new Rx.Subject()
  broker.events = new Rx.Subject()

  return broker
}

export async function start (broker) {
  broker.symKeyID = await generateWhisperSymKeyAsync(broker)
  broker.powTarget = await getCurrentPowAsync(broker)

  startOutgoingMessageProcessing(broker)
  await startIncomingMessageProcessing(broker)

  return broker
}

export async function stop (broker) {
  await Promise.all(
    broker.subscriptions.map(s => s.unsubscribe())
  )

  broker.subscriptions = []

  if (!R.isNil(broker.topicFilters)) {
    const host = broker.web3.currentProvider.host

    await Promise.all(
      broker.topicFilters.map(filter => deleteMessageFilters(host, filter))
    )
    broker.topicFilters = []
  }

  return broker
}

export function toWhisperEnvelope (broker, message) {
  return WhisperEnvelope({
    payload: encodeMessageAsPayload(message),
    topic: TOPICS.OAX_ORDERS,
    powTarget: broker.powTarget,
    symKeyID: broker.symKeyID
  })
}

// PRIVATE FUNCTIONS

async function generateWhisperSymKeyAsync (broker) {
  const password = tokenPairToHash(broker.tokenPair)
  return broker.web3.shh.generateSymKeyFromPassword(password)
}

async function getCurrentPowAsync (broker) {
  const whisperInfo = await broker.web3.shh.getInfo()
  return whisperInfo.minPow
}

async function createTopicFiltersAsync (broker, topics) {
  const { web3, symKeyID } = broker
  return Promise.all(
    topics.map(topic =>
      web3.shh.newMessageFilter({ topics: [topic], symKeyID })
    )
  )
}

function startOutgoingMessageProcessing (broker) {
  const subscription = broker.sendChannel.subscribe({
    next: message => postMessage(broker, message),
    error: err => console.error(`MessageBroker stopped due to error:`, err),
    complete: () => console.debug('Message Broker send channel closed.')
  })

  broker.subscriptions.push(subscription)
}

async function postMessage (broker, message) {
  const validatedMessage = Message(message)
  const envelope = toWhisperEnvelope(broker, validatedMessage)

  let result, error

  try {
    result = await postAsync(broker, envelope)
  } catch (err) {
    error = err
  }

  logMessageConfirmation(broker, { message, result, error })
}

async function postAsync (broker, message, retry) {
  if (R.isNil(retry)) retry = broker.powRetry

  try {
    await broker.web3.shh.post(message)
  } catch (err) {
    if (retry === 0) throw err

    const retryMessage = await increaseMessagePowTargetAsync(broker, message)

    return postAsync(broker, retryMessage, retry - 1)
  }
}

async function increaseMessagePowTargetAsync (broker, message) {
  const powTarget = (await getCurrentPowAsync(broker)) + 1
  return R.merge(message, { powTarget })
}

async function startIncomingMessageProcessing (broker) {
  let subscription

  if (isWeb3ProviderSubscriptionCapable(broker.web3)) {
    subscription = subscirbeToMessages(broker).subscribe(
      msg => broker.receiveChannel.next(msg),
      err => console.error(err)
    )
  } else {
    const topics = [TOPICS.OAX_ORDERS]
    broker.topicFilters = await createTopicFiltersAsync(broker, topics)

    subscription = WhisperMessage(broker).subscribe(
      msg => broker.receiveChannel.next(msg),
      err => console.error(err)
    )
  }

  broker.subscriptions.push(subscription)
}

function subscirbeToMessages (broker) {
  return Rx.Observable.create(observer => {
    const subscription = broker.web3.shh.subscribe(
      'messages',
      {
        symKeyID: broker.symKeyID,
        topics: [TOPICS.OAX_ORDERS]
      },
      function (err, message) {
        if (err) { throw err }
        observer.next(message)
      })

    return function () {
      subscription.unsubscribe()
    }
  }).map(decodeMessage(broker))
}

function WhisperMessage (broker) {
  return Rx.Observable.interval(broker.pollInterval)
    .mergeMap(() => broker.topicFilters)
    .mergeMap(getFilterMessages(broker))
    .mergeMap(convertMessageArrayToMessages)
    .map(decodeMessage(broker))
}

function getFilterMessages (broker) {
  return async filter => {
    const host = broker.web3.currentProvider.host

    let rpcRequestObject = {
      body: JSON.stringify({
        jsonrpc: '2.0',
        method: 'shh_getFilterMessages',
        params: [filter],
        id: Web3.utils.randomHex(32)
      }),
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST'
    }

    let messages = []

    try {
      const response = await fetch(host, rpcRequestObject) // eslint-disable-line no-undef

      messages = (await response.json()).result
    } catch (err) {
      console.error(err)
    }

    return messages
  }
}

function convertMessageArrayToMessages (messages) {
  return Rx.Observable.from(messages)
}

function decodeMessage (broker) {
  return envelope => {
    try {
      return Message(decodePayloadToMessage(envelope.payload))
    } catch (error) {
      logErrorMessage(broker, { error, data: envelope })
    }
  }
}

function logMessageConfirmation (broker, { message, result, error }) {
  broker.sendMessageConfirmation.next({ message, result, error })
}

function logErrorMessage (broker, { error, data }) {
  broker.events.next({ type: 'error', error, data })
}

function encodeMessageAsPayload (message) {
  return utf8ToHex(JSON.stringify(message))
}

function decodePayloadToMessage (payload) {
  return JSON.parse(hexToUtf8(payload))
}

async function deleteMessageFilters (host, filter) {
  console.debug('[MessageBroker] Removing message filter', filter)

  if (typeof window !== 'undefined' && window.Worker) {
    // eslint-disable-next-line no-undef
    const worker = new Worker('./dex/messaging/DeleteMessageFilterWorker.js')
    worker.postMessage([host, filter])
  } else {
    let rpcRequestObject = {
      body: JSON.stringify({
        jsonrpc: '2.0',
        method: 'shh_deleteMessageFilter',
        params: [filter],
        id: Math.trunc(Math.random() * 1e16)
      }),
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST'
    }

    // eslint-disable-next-line no-undef
    const req = fetch(host, rpcRequestObject)

    return req
  }
}
