/* An Order is just a POJO, handled by functions in this module */
import {
  address,
  allowance,
  balance,
  D2wad,
  distillEvent,
  now,
  sendNoReceipt,
  sigCoords,
  wad2D,
  ZERO_ADDR
} from '../lib/chain-dsl/es6.js'
import { Assert, dontObserve, struct } from './utils.js'
import * as FillIntent from './FillIntent.js'

const OrderSchema = {
  exp: 'number',
  events: ['object'],
  cancels: ['object'],
  buy: {
    amount: 'orderAmount',
    token: {
      address: 'address',
      name: 'string',
      symbol: 'string'
    }
  },
  sell: {
    amount: 'orderAmount',
    token: {
      address: 'address',
      name: 'string',
      symbol: 'string'
    }
  },
  address: 'string',
  salt: 'number',
  orderHash: 'hash?',
  sig: 'signature?'
}

const SignedOrderSchema = {
  ...OrderSchema,
  orderHash: 'hash',
  sig: 'signature'
}

export const WhisperOrderSchema = R.pick([
  'address',
  'sell',
  'buy',
  'exp',
  'salt',
  'sig',
  'orderHash'
], SignedOrderSchema)

export const WHISPER_FIELDS = R.keys(WhisperOrderSchema)

/**
 * Validates the Order params using `superstruct`
 */
export const OrderValidator = struct(
  OrderSchema,
  // DEFAULTS
  {
    events: [],
    cancels: []
  }
)

/**
 * Validates the Order params using `superstruct`
 */
export const SignedOrderValidator = struct(
  SignedOrderSchema,
  // DEFAULTS
  {
    events: [],
    cancels: []
  }
)

export class Order {
  constructor (params) {
    const defaults = {
      exp: now() + 3600 * 24, // 1 day
      events: [],
      cancels: [],
      salt: Math.trunc(Math.random() * 1e20)
    }

    const amounts2D = {
      buy: {amount: D},
      sell: {amount: D}
    }

    const order = OrderValidator(R.evolve(amounts2D, {...defaults, ...params}))

    // mutate `this`
    Object.assign(this, order)
    dontObserve(this, 'exp')
    dontObserve(this, 'buy')
    dontObserve(this, 'sell')
    dontObserve(this, 'address')
    dontObserve(this, 'salt')
    Object.freeze(this)
  }

  get filled () {
    const amounts = R.map(filledAmounts, this.events)
    const filled = {
      'buy': sum(R.map(R.prop('buy'), amounts)),
      'sell': sum(R.map(R.prop('sell'), amounts))
    }
    return filled
  }

  get status () {
    const filledBuy = this.filled.buy
    const lastEvent = R.last(this.events)
    if (lastEvent) {
      if (lastEvent.status === 'error' ||
        lastEvent.status === 'expired' ||
        lastEvent.status === 'cancelled') {
        return lastEvent.status
      }
    }

    if (filledBuy.gte(this.buy.amount)) {
      return 'fulfilled'
    } else if (filledBuy.gt(D`0`)) {
      return 'partiallyFilled'
    } else if (this.exp > now()) {
      return 'active'
    } else {
      return 'expired'
    }
  }
}

function sum (numbers) {
  // could not make this work with R.reduce
  return numbers.reduce((acc, val) => acc.plus(val), D`0`)
}

export function tokensAddress (order) {
  return [
    order.buy.token.address,
    order.sell.token.address
  ]
}

export function hasTokenPair (order, tokenPair) {
  const orderTokensAddress = tokensAddress(order)

  const hasBaseToken = orderTokensAddress.includes(tokenPair.base.address)
  const hasQuoteToken = orderTokensAddress.includes(tokenPair.quote.address)

  return hasBaseToken && hasQuoteToken
}

export function WhisperOrder (orderParams) {
  const order = create(orderParams)
  return R.pick(WHISPER_FIELDS, order)
}

export function createFromPrice ({amount, price, tokenPair, intentType, ...otherParams}) {
  const intent = {buy: {}, sell: {}}
  const [base, quote] = (intentType === 'buy') ? ['buy', 'sell'] : ['sell', 'buy']

  intent[base].token = tokenPair.base
  intent[base].amount = D(amount)
  intent[quote].token = tokenPair.quote
  intent[quote].amount = D(amount).times(D(price))

  const orderParams = {...otherParams, ...intent}
  return create(orderParams)
}

export function withPrice (order, tokenPair) {
  const isBuyingBase = (order.buy.token.address === tokenPair.base.address)

  const [base, quote] = isBuyingBase ? ['buy', 'sell'] : ['sell', 'buy']
  const amount = order[quote].amount.div(order[base].amount)

  return merge(order, {price: {amount, tokenPair}})
}

/*
* Create an order object with all the possible fields
* and provides some convenient defaults for both
* automated and manual test
* */
export function create (params) {
  return new Order(params)
}

export function fromTradeIntent (tradeIntent) {
  const {intentType, ...orderParams} = R.clone(tradeIntent)
  return create(orderParams)
}

export const isNotExpired = ({exp}) => exp > now()

export const ZeroExErrors = [
  'ORDER_EXPIRED',
  'ORDER_FULLY_FILLED_OR_CANCELLED',
  'ROUNDING_ERROR_TOO_LARGE',
  'INSUFFICIENT_BALANCE_OR_ALLOWANCE'
]

export function asArrays (makerOrder) {
  // It prepares the parameters for this `Exhcange.fillOrder` function:
  /*
    /// @dev Fills the input order.
    /// @param orderAddresses Array of order's maker, taker, makerToken, takerToken, and feeRecipient.
    /// @param orderValues Array of order's makerTokenAmount, takerTokenAmount, makerFee, takerFee, expirationTimestampInSec, and salt.
    /// @param fillTakerTokenAmount Desired amount of takerToken to fill.
    /// @param shouldThrowOnInsufficientBalanceOrAllowance Test if transfer will fail before attempting.
    /// @param v ECDSA signature parameter v.
    /// @param r ECDSA signature parameters r.
    /// @param s ECDSA signature parameters s.
    /// @return Total amount of takerToken filled in trade.

    function fillOrder(
      address[5] orderAddresses,
      uint[6] orderValues,
      uint fillTakerTokenAmount,
      bool shouldThrowOnInsufficientBalanceOrAllowance,
      uint8 v,
      bytes32 r,
      bytes32 s)
    public
    returns (uint filledTakerTokenAmount)
    {
      Order memory order = Order({
      maker : orderAddresses[0],
      taker : orderAddresses[1],
      makerToken : orderAddresses[2],
      takerToken : orderAddresses[3],
      feeRecipient : orderAddresses[4],
      makerTokenAmount : orderValues[0],
      takerTokenAmount : orderValues[1],
      makerFee : orderValues[2],
      takerFee : orderValues[3],
      expirationTimestampInSec : orderValues[4],
      orderHash : getOrderHash(orderAddresses, orderValues)
    });
    ...
  */
  return {
    orderAddresses: [
      makerOrder.address,
      ZERO_ADDR, // any taker
      makerOrder.sell.token.address,
      makerOrder.buy.token.address,
      ZERO_ADDR // no fee recipient
    ],
    orderValues: [
      D2wad(makerOrder.sell.amount),
      D2wad(makerOrder.buy.amount),
      toBN(0), // no maker fee
      toBN(0), // no taker fee
      toBN(makerOrder.exp),
      toBN(makerOrder.salt)
    ]
  }
}

export function hash (contracts, order) {
  const asAddress = (value) => { return {t: 'address', value} }
  const asUint = (value) => { return {t: 'uint', value} }

  const {orderAddresses, orderValues} = asArrays(order)
  const orderPayload = [
    asAddress(address(contracts.xchg)),
    ...orderAddresses.map(asAddress),
    ...orderValues.map(asUint)
  ]
  return Web3.utils.soliditySha3(...orderPayload)
}

export function withHash (contracts, order) {
  return create(merge(order, {orderHash: hash(contracts, order)}))
}

export async function sign (web3, contracts, order) {
  const orderHash = hash(contracts, order)
  let hashToSign

  if (web3.currentProvider.isMetaMask) {
    hashToSign = web3.eth.accounts.hashMessage(orderHash)
  } else {
    hashToSign = orderHash
  }

  const sig = await web3.eth.sign(hashToSign, order.address)
  return create(merge(order, {sig, orderHash}))
}

export function findContract (contracts, addr) {
  return Object.values(contracts).find(c => address(c) === addr)
}

async function ensureValidOrders (
  web3,
  contracts,
  {address: takerWallet, order: makerOrder, sellAmt, buyAmt}
) {
  const fillTakerTokenAmount = D2wad(sellAmt)
  const fillMakerTokenAmount = D2wad(buyAmt)

  if (makerOrder.address.toLowerCase() === takerWallet.toLowerCase()) {
    throw new Error('Maker and taker cannot be the same address')
  }

  const {orderAddresses} = asArrays(makerOrder)

  const makerToken = findContract(contracts, orderAddresses[2])
  const takerToken = findContract(contracts, orderAddresses[3])

  if (address(makerToken) === address(takerToken)) {
    throw new Error('Maker and taker tokens cannot be the same address')
  }

  const makerBalance = toBN(
    await makerToken.methods.balanceOf(makerOrder.address).call())
  if (makerBalance.lt(fillMakerTokenAmount)) {
    throw new Error(
      `Maker token balance (${makerBalance}) less than maker amount (${fillMakerTokenAmount.toString()})`)
  }

  const takerBalance = await takerToken.methods.balanceOf(takerWallet)
    .call()
  if (fillTakerTokenAmount.gt(toBN(takerBalance))) {
    throw new Error('Taker token balance less than taker amount')
  }

  const makerAllowance = await makerToken.methods.allowance(makerOrder.address,
    address(contracts.proxy)).call()
  if (fillMakerTokenAmount.gt(toBN(makerAllowance))) {
    throw new Error('Maker token allowance less than maker amount')
  }

  const takerAllowance = await takerToken.methods.allowance(takerWallet,
    address(contracts.proxy)).call()
  if (fillTakerTokenAmount.gt(toBN(takerAllowance))) {
    throw new Error('Taker token allowance less than taker amount')
  }

  const takerEther = await web3.eth.getBalance(takerWallet)
  if (toBN(90000).gt(toBN(takerEther))) {
    throw new Error('Taker ether balance too low')
  }

  // const makerAddr = await web3.eth.personal.ecRecover(hash(contracts, makerOrder), makerOrder.sig)
  // if (makerAddr !== makerOrder.address)
  //     throw new Error("Maker signature invalid")
}

function decodeExchangeError (errorId) {
  const ExchangeErrors = [
    'ORDER_EXPIRED - Order has already expired',
    'ORDER_FULLY_FILLED_OR_CANCELLED - Order has already been fully filled or cancelled',
    'ROUNDING_ERROR_TOO_LARGE - Rounding error too large',
    'INSUFFICIENT_BALANCE_OR_ALLOWANCE - Insufficient balance or allowance for token transfer'
  ]
  return ExchangeErrors[errorId] || 'Unknown Exchange error'
}

export function matchIntentWithMakerOrders (tradeIntent, matchingOrders) {
  let pendingBuyAmt = tradeIntent.buy.amount
  let pendingSellAmt = tradeIntent.sell.amount

  const fillIntents = []

  for (const {order, fillableSellAmt} of matchingOrders) {
    const {sellAmt, buyAmt} = settlementAmounts(
      order, fillableSellAmt, pendingBuyAmt, pendingSellAmt, tradeIntent.intentType
    )

    if (buyAmt.lte(D`0`)) { continue }

    const fillIntent = FillIntent.create({
      order,
      address: tradeIntent.address,
      buyAmt,
      sellAmt
    })
    fillIntents.push(fillIntent)

    pendingBuyAmt = pendingBuyAmt.minus(buyAmt)
    pendingSellAmt = pendingSellAmt.minus(sellAmt)

    if (pendingSellAmt.lt(D`0`)) {
      throw new Error(`Overspent: pendingSellAmt=${pendingBuyAmt}`)
    } else if (tradeIntent.intentType === 'buy' && pendingBuyAmt.eq(D`0`)) {
      break
    } else if (tradeIntent.intentType === 'sell' && pendingSellAmt.eq(D`0`)) {
      break
    }
  }
  return fillIntents
}

export function makerOrderFromIntents (tradeIntent, fillIntents) {
  function total (prop) {
    return sum(R.map(R.prop(prop), fillIntents))
  }

  const price = tradeIntent.buy.amount.div(tradeIntent.sell.amount)

  let buy, sell
  if (tradeIntent.intentType === 'buy') {
    const target = tradeIntent.buy.amount
    const filled = total('buyAmt')
    buy = target.minus(filled)
    sell = buy.div(price)
  } else if (tradeIntent.intentType === 'sell') {
    const target = tradeIntent.sell.amount
    const filled = total('sellAmt')
    sell = target.minus(filled)
    buy = sell.times(price)
  } else {
    throw new Error(`invalid intentType ${tradeIntent.intentType}`)
  }

  if (buy.gt(D`0`) && sell.gt(D`0`)) {
    const {intentType, ...orderParams} = R.clone(tradeIntent)
    orderParams.buy.amount = buy
    orderParams.sell.amount = sell

    return [create(orderParams)]
  } else {
    return []
  }
}

/**
 * @param Order makerOrder
 * @param BN fillableSellAmt - amount the maker has available to sell to taker
 * @param BN takerBuyAmt - remaining amount the taker would like to buy
 * @param BN takerSellAmt - remaining amount the taker would like to sell
 * @param string intentType - if taker is BUY or SELL
 *
 * returns {buyAmt} - amount taker should buy
 * returns {sellAmt} - amount taker should sell
 */
export function settlementAmounts (makerOrder, fillableSellAmt, takerBuyAmt, takerSellAmt, intentType) {
  // buy and sell is interpreted from the taker's point of view

  const price = makerOrder.buy.amount.div(makerOrder.sell.amount)
  let buyAmt, sellAmt

  if (intentType === 'buy') {
    // We as taker can only buy as much as the maker wants to sell.
    buyAmt = BigNumber.min(fillableSellAmt, takerBuyAmt)
    sellAmt = buyAmt.times(price)
  } else if (intentType === 'sell') {
    // Taker may buy more than intent specifies if price is better.

    const fillableTakerBuyAmt = fillableSellAmt
    const fillableTakerSellAmt = fillableTakerBuyAmt.times(price)

    sellAmt = BigNumber.min(fillableTakerSellAmt, takerSellAmt)

    buyAmt = sellAmt.div(price)
  } else {
    throw new Error(`invalid intentType ${intentType}`)
  }
  return {buyAmt, sellAmt}
}

export async function settle (web3, contracts, fillIntent) {
  const {order: makerOrder, address: takerWallet, sellAmt} = fillIntent

  // The takerToken is what the taker is selling
  const fillTakerTokenAmount = D2wad(sellAmt)

  await ensureValidOrders(web3, contracts, fillIntent)

  const {orderAddresses, orderValues} = asArrays(makerOrder)
  const {v, r, s} = sigCoords(makerOrder.sig)

  if (await web3.eth.net.getId() === 1) {
    throw Error('Not ready for mainnet yet.')
  }
  const transactionHash = await sendNoReceipt(
    contracts.xchg, takerWallet,
    'fillOrder', orderAddresses, orderValues,
    fillTakerTokenAmount, true, v, r, s)

  return transactionHash
}

export async function cancel (web3, contracts, cancelIntent) {
  const {order, cancelAmt} = cancelIntent
  const cancelTakerTokenAmount = D2wad(cancelAmt)
  const {orderAddresses, orderValues} = asArrays(order)
  console.debug(`Cancelling ${cancelAmt} of ${order}`)

  if (await web3.eth.net.getId() === 1) {
    throw Error('Not ready for mainnet yet.')
  }

  const transactionHash = await sendNoReceipt(
    contracts.xchg, order.address,
    'cancelOrder', orderAddresses, orderValues,
    cancelTakerTokenAmount)
  return transactionHash
}

function containsEvent (event, order) {
  return order.events.find(e => e.transactionHash === event.transactionHash)
}

export async function updateFromEvent (event, order, contracts) {
  if (containsEvent(event, order)) { return order }

  let newOrder = R.clone(order)
  Object.setPrototypeOf(newOrder, Object.getPrototypeOf(order))

  let parsedEvent = event

  if (event.event === 'LogError') {
    parsedEvent = await parseErrorEvent(event, order, contracts)
  } else if (event.event === 'LogCancel') {
    parsedEvent = {...event, status: 'cancelled'}
  }

  newOrder.events.push(parsedEvent)

  return newOrder
}

function isMakerOfEvent (order, event) {
  return order.orderHash === event.returnValues.orderHash
}

async function parseErrorEvent (event, order, contracts) {
  const distilledError = distillEvent(event)
  const errorId = distilledError.errorId
  distilledError.message = decodeExchangeError(errorId)

  const error = ZeroExErrors[errorId]

  let msg, status

  if (error === 'ORDER_EXPIRED') {
    status = 'expired'
  } else if (error === 'ORDER_FULLY_FILLED_OR_CANCELLED' && isMakerOfEvent(order, event)) {
    // noop for maker
  } else if (error === 'ROUNDING_ERROR_TOO_LARGE' && isMakerOfEvent(order, event)) {
    // noop for maker
  } else if (error === 'INSUFFICIENT_BALANCE_OR_ALLOWANCE') {
    const makerTokenContract = findContract(contracts, order.sell.token.address)

    const makerBalance = await balance(makerTokenContract, order.address)
    const makerAllowance = await allowance(makerTokenContract, order.address, address(contracts.proxy))

    const makerAmount = D2wad(order.sell.amount)

    if (makerAmount.gt(makerBalance) || makerAmount.gt(makerAllowance)) {
      status = 'error'
      msg = distilledError
      console.warn(distilledError)
    }
  } else {
    status = 'error'
    msg = distilledError
    console.warn(distilledError)
  }

  return {...event, msg, status}
}

function filledAmounts (event) {
  // This is an order so we are the maker.
  // MakerToken is the token the maker has (sells).

  const sell = wad2D(R.path(['returnValues', 'filledMakerTokenAmount'], event) || '0')
  const buy = wad2D(R.path(['returnValues', 'filledTakerTokenAmount'], event) || '0')

  return {buy, sell}
}

function bestPriceFirst (order1, order2) {
  // for sorting
  // f(a, b)
  //     a < b: return negative
  //     a = b: return 0
  //     a > b: return positive
  // if result is negative a comes before b
  Assert.ok(order1.sell.token.address, order2.sell.token.address)
  Assert.ok(order1.buy.token.address, order2.buy.token.address)
  return sellPrice(order1).minus(sellPrice(order2))
}

function buyPrice (order) {
  return order.sell.amount.div(order.buy.amount)
}

function sellPrice (order) {
  return order.buy.amount.div(order.sell.amount)
}

function areOppositePairs (order1, order2) {
  return order2.buy.token.address === order1.sell.token.address &&
    order2.sell.token.address === order1.buy.token.address
}

export function matchingOrders (orderbookOrders, tradeIntent) {
  const takerOrderPrice = sellPrice(tradeIntent)
  return orderbookOrders
    .filter(makerOrder => areOppositePairs(tradeIntent, makerOrder))
    .filter(makerOrder => buyPrice(makerOrder).gte(takerOrderPrice))
    .sort(bestPriceFirst)
}

/**
 * @param {Array<Order>} orders - array of matching orders
 * @param {Map<String,BN>} available - max taker amount to fill for each address
 *
 * returns {Array<Order} with taker token available
 */
export function availableForFill (orders, fillableMakerTokenAmounts, availableTakerTokenAmounts) {
  return R.pipe(
    R.groupBy(order => order.address),
    R.toPairs,
    R.map(
      ([address, ordersOfAddress]) => {
        let fillableMakerTokenAmount = fillableMakerTokenAmounts.get(address) || D`0`

        return R.sort(bestPriceFirst, ordersOfAddress).map(
          order => {
            const availableTakerTokenAmount = availableTakerTokenAmounts.get(order.orderHash)
            const availableMakerTokenAmount = availableTakerTokenAmount.times(order.sell.amount).div(order.buy.amount)

            const fillableSellAmt = BigNumber.max(D`0`, BigNumber.min(fillableMakerTokenAmount, availableMakerTokenAmount))
            const fillableBuyAmt = fillableSellAmt.times(order.buy.amount).div(order.sell.amount)

            fillableMakerTokenAmount = fillableMakerTokenAmount.minus(fillableSellAmt)
            return {
              order,
              fillableSellAmt,
              fillableBuyAmt
            }
          }
        )
      }
    ),
    R.flatten,
    R.sort((a, b) => bestPriceFirst(a.order, b.order))
  )(orders)
}
