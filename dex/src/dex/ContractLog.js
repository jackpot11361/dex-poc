import { struct } from './utils.js'

const ContractLog = struct(
  {
    web3: 'object',
    exchange: 'object',
    storage: 'any',
    netId: 'number',

    pollInterval: 'number',
    fromBlock: 'number',
    historySize: 'number'
  },
  // DEFAULT
  {
    pollInterval: 1000,
    fromBlock: 0,
    historySize: 200
  }
)

export function create (params) {
  const contractLog = ContractLog(params)
  contractLog.storageKey = `lastSeenBlock-${contractLog.netId}`

  return contractLog
}

export async function start (contractLog) {
  contractLog.fromBlock = await calculateStartingBlock(contractLog)
  const exchange = contractLog.exchange
  const pastEvents = R.partial(getPastEvents, [exchange, 'allEvents'])

  const events = Rx.Observable.interval(contractLog.pollInterval)
    .mergeMap(() => pastEvents({fromBlock: contractLog.fromBlock}))
    .mergeMap(events => Rx.Observable.from(events))
    .distinct(event => JSON.stringify(event))
    .share()

  contractLog.events = events

  contractLog.updateAndStoreBlockNumber = events.subscribe(
    event => {
      const storageKey = contractLog.storageKey
      contractLog.storage.setItem(storageKey, event.blockNumber)
    }
  )
}

export function stop (contractLog) {
  contractLog.updateAndStoreBlockNumber.unsubscribe()
}

export function getPastEvents (exchange, ...args) {
  return Rx.Observable.fromPromise(exchange.getPastEvents(...args))
}

async function calculateStartingBlock (contractLog) {
  const { web3, storage, storageKey, historySize } = contractLog

  const currentBlock = await web3.eth.getBlockNumber()
  const storedBlock = parseInt(storage.getItem(storageKey))
  const lastSeenBlock = storedBlock || currentBlock
  const startOfHistoryBlock = Math.max(currentBlock - historySize, 0)

  return Math.min(startOfHistoryBlock, lastSeenBlock)
}
