export class Trade {
  constructor ({tradeIntent, fills, orders}) {
    this.tradeIntent = tradeIntent
    this.fills = fills
    this.orders = orders

    Object.freeze(this)
  }
}

export function create ({tradeIntent, fills, orders}) {
  return new Trade({tradeIntent, fills, orders})
}
