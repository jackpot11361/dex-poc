/* eslint-disable no-undef */
import { address, balance, wad2D } from './lib/chain-dsl/es6.js'
import { makeChannel, isWeb3ProviderSubscriptionCapable } from './dex/utils.js'
import { SystemMap } from './component.js'
import * as Order from './dex/Order.js'
import * as Orderbook from './dex/Orderbook.js'
import * as MessageBroker from './dex/messaging/MessageBroker.js'
import * as TradeHistory from './dex/TradeHistory.js'
import * as Blotter from './dex/Blotter.js'
import * as OrderManager from './dex/OrderManager.js'
import * as ContractLog from './dex/ContractLog.js'
import * as Contracts from './dex/Contracts.js'
import * as CancelIntent from './dex/CancelIntent.js'
import * as TradeIntent from './dex/TradeIntent.js'
import * as Fill from './dex/Fill.js'
import * as Trade from './dex/Trade.js'
import { config } from './config.js'
import { toOrderMessage } from './dex/messaging/Message.js'

export const defaultTokenPairNames = [
  'WETH/SWIMUSD',
  'OAX/WETH'
]

const tokenPairFromName = R.curry((tokens, tokenPairName) => {
  const [baseSym, quoteSym] = tokenPairName.split('/')
  return {
    base: tokens[baseSym],
    quote: tokens[quoteSym],
    key: [baseSym, quoteSym].join('_'),
    label: [baseSym, quoteSym].join('/'),
    priceLabel: [quoteSym, baseSym].join('/')
  }
})

function createStore (dex) {
  return {
    state () {
      return {
        selectedPair: dex.selectedPair,
        orderPrice: D`0`,
        orderAmount: D`0`,
        orderType: 'buy'
      }
    },

    getters: {
      accounts: () => { return () => { return dex.accounts } },
      tokenPairs: () => dex.tokenPairs,
      orderTotal: (state) => state.orderPrice * state.orderAmount
    },

    mutations: {
      selectPair (state, pair) {
        state.selectedPair = pair
        dex.selectedPair = pair
      },
      setOrderAmount (state, amt) { state.orderAmount = D(amt || '0') },
      setOrderPrice (state, price) { state.orderPrice = D(price || '0') },
      setOrderType (state, type) { state.orderType = type }
    },

    actions: {
      async placeOrder ({state, getters}) {
        const accounts = await dex.mmWeb3.eth.getAccounts()

        if (Array.isArray(accounts) && accounts.length === 0) {
          return window.alert('Please ensure that your MetaMask account is unlocked')
        }

        const {
          selectedPair,
          orderType,
          orderPrice,
          orderAmount
        } = state

        if (orderPrice.isZero() || orderAmount.isZero()) return

        const tradeIntent = TradeIntent.fromPrice({
          intentType: orderType,
          amount: orderAmount,
          price: orderPrice,
          tokenPair: selectedPair,
          address: getters.accounts().selected
        })

        try {
          return await placeOrder(dex, tradeIntent)
        } catch (err) {
          alert(err.message)
        }
      },

      async cancelOrder (context, order) {
        return cancelOrder(dex, order)
      }
    }
  }
}

async function userHasBalanceAndAllowance (dex, tradeIntent) {
  const makerTokenContract = Order.findContract(
    dex.contracts,
    tradeIntent.sell.token.address
  )

  const makerBalance = wad2D(await balance(makerTokenContract, tradeIntent.address))
  const allowance = await makerTokenContract.methods.allowance(tradeIntent.address, address(dex.contracts.proxy)).call()

  const approved = D(allowance).gt(0)

  const hasBalance = makerBalance.gte(tradeIntent.sell.amount)

  return { approved, hasBalance }
}

export class System extends SystemMap {
  constructor ({
    chainUrl = devChainUrl,
    mmProvider,
    ContractLoader,
    searchParams = {},
    storage,
    tokenPairNames = defaultTokenPairNames,
    whisperPollInterval = 8000,

    mmWeb3,
    whisperWeb3,
    contracts,
    contractLog,
    contractEvents
  }) {
    super()

    if ((mmProvider == null) || ('nomm' in searchParams)) {
      mmWeb3 = new Web3(chainUrl)
    } else {
      mmWeb3 = new Web3(mmProvider)
    }

    const selectedPairName = searchParams.pair || tokenPairNames[0]

    Object.assign(this, {
      chainUrl,
      mmProvider,
      ContractLoader,
      searchParams,
      storage,
      tokenPairNames,
      whisperPollInterval,
      config: R.clone(config),

      selectedPairName,
      mmWeb3,
      contracts,
      whisperWeb3,
      contractLog,
      contractEvents
    })
  }

  async start () {
    console.debug('Initializing OAX DEX')
    let {
      chainUrl,
      mmProvider,
      ContractLoader,
      searchParams,
      config,

      store,
      storage,
      tokenPairNames,
      selectedPairName,
      mmWeb3,
      netId,
      accounts,
      whisperWeb3,
      contracts,
      tokens,
      orderbook,
      blotter,
      tradeHistory,
      contractLog,
      contractEvents,
      sendMessageChannel,
      receiveMessageChannel,
      whisperPollInterval
    } = this

    await logWeb3Status('Metamask', mmWeb3)

    // FIXME `netId` and `config.netId` can be inconsistent
    netId = await mmWeb3.eth.net.getId()
    this.netId = config.networkId

    accounts = await getAccounts(mmWeb3)

    const whisperNodeUrl = config.whisperNodeUrl || this.chainUrl
    whisperWeb3 = whisperWeb3 || new Web3(whisperNodeUrl)
    await logWeb3Status('Whisper', whisperWeb3)

    if (!contracts) {
      const contractLoader = ContractLoader(this.netId)
      contracts = await Contracts.load(contractLoader, mmWeb3)
    }

    tokens = {
      'WETH': {
        symbol: 'WETH',
        name: 'Demo Wrapped Ether',
        address: address(contracts.weth)
      },
      'OAX': {
        symbol: 'OAX',
        name: 'Demo Open Asset Exchange',
        address: address(contracts.oax)
      },
      'SWIMUSD': {
        symbol: 'SWIMUSD',
        name: 'Demo SWIM USD',
        address: address(contracts.swimusd)
      },
      'ZRX': {
        symbol: 'ZRX',
        name: '0x Protocol Token',
        address: address(contracts.zrx)
      }
    }
    // FIXME This is unintuitive, but the store creation needs tokens already available
    this.tokens = tokens

    this.tokenPairs = tokenPairNames.map(tokenPairFromName(tokens))

    const selectedPair = tokenPairFromName(tokens, selectedPairName)
    this.selectedPair = selectedPair

    store = store || new Vuex.Store(createStore(this))

    sendMessageChannel = makeChannel()
    receiveMessageChannel = makeChannel()

    let messageBroker = MessageBroker.create({
      web3: whisperWeb3,
      tokenPair: this.selectedPair,
      sendChannel: sendMessageChannel,
      receiveChannel: receiveMessageChannel,
      pollInterval: whisperPollInterval
    })
    await MessageBroker.start(messageBroker)

    orderbook = orderbook || Orderbook.create({
      store,
      storage,
      netId,
      sendMessageChannel,
      receiveMessageChannel,
      web3: mmWeb3,
      contracts,
      tokenPair: this.selectedPair
    })

    orderbook = Orderbook.start(orderbook)

    blotter = blotter || Blotter.create({
      store,
      storage,
      netId,
      sendMessageChannel,
      receiveMessageChannel
    })

    blotter = Blotter.start(blotter)

    contractLog = contractLog || ContractLog.create({
      web3: mmWeb3,
      exchange: contracts.xchg,
      netId,
      storage,
      historySize: 6 * 30 * 24 * 60 * 60 / 15 // 6 months with 15 sec block time
    })

    await ContractLog.start(contractLog)

    contractEvents = contractLog.events

    tradeHistory = TradeHistory.create({store, web3: mmWeb3, contractEvents})
    tradeHistory = TradeHistory.start(tradeHistory)

    let orderManager = OrderManager.create(
      {blotter, store, contracts, web3: mmWeb3})

    orderManager = {
      ...orderManager,
      web3: mmWeb3,
      orderbook,
      contractEvents
    }
    orderManager = OrderManager.start(orderManager)

    const cancelAccountCheck = setInterval(async () => {
      const accounts = await this.mmWeb3.eth.getAccounts()

      if (Array.isArray(accounts) && accounts.length > 0) {
        this.accounts = await getAccounts(this.mmWeb3)
        clearInterval(cancelAccountCheck)
      }
    }, 200)

    // Object.assign is a simple way to preserve the stop method
    return Object.assign(this, {
      chainUrl,
      mmProvider,
      searchParams,

      store,
      storage,
      selectedPair,
      mmWeb3,
      netId,
      accounts,
      whisperWeb3,
      contracts,
      tokens,
      orderbook,
      messageBroker,
      blotter,
      tradeHistory,
      orderManager,
      contractEvents,
      contractLog,
      sendMessageChannel,
      receiveMessageChannel
    })
  }

  async stop () {
    await Orderbook.stop(this.orderbook)
    await Blotter.stop(this.blotter)
    await TradeHistory.stop(this.tradeHistory)
    await OrderManager.stop(this.orderManager)
    await ContractLog.stop(this.contractLog)
    await MessageBroker.stop(this.messageBroker)

    if (isWeb3ProviderSubscriptionCapable(this.whisperWeb3)) {
      this.whisperWeb3.currentProvider.connection.close()
    }

    return this
  }
}

async function getAccounts (web3) {
  const accounts = {
    list: await web3.eth.getAccounts(),
    get selected () {
      return this.list[0]
    }
  }

  // If there are more than 1 account is available, we must be
  // running against a dev geth node with demo accounts
  if (accounts.list.length > 1) {
    accounts.DEPLOYER = accounts.list[0]
    accounts.ALICE = accounts.list[1]
    accounts.BOB = accounts.list[2]
  }
  return accounts
}

export async function logWeb3Status (providerName, web3) {
  console.debug(`${providerName}: Using web3 version ${web3.version}`)
  const networkId = await web3.eth.net.getId()
  console.debug(`${providerName}: Connected to NetworkID ${networkId}`)
}

function addTradeToBlotter (dex, trade) {
  trade.orders.forEach(order => dex.store.commit('blotter/addRecord', order))
  trade.fills.forEach(fill => dex.store.commit('blotter/addRecord', fill))
}

export async function placeOrder (dex, tradeIntent) {
  await preventOverOrder(dex, tradeIntent)

  const eligibleOrders = dex.store.state.orderbook.orders
    .filter(Order.isNotExpired)
    .filter(order => order.address !== tradeIntent.address)

  const matches = Order.matchingOrders(eligibleOrders, tradeIntent)

  const uniqueAddressesForMatches = R.uniq(matches.map(o => o.address))

  const tokenContract = Order.findContract(dex.contracts, tradeIntent.buy.token.address)

  const fillableMakerTokenAmounts = new Map(await Promise.all(
    uniqueAddressesForMatches.map(async a =>
      getFillableMakerTokenAmountAsync(a, tokenContract, dex.contracts.proxy)
    )
  ))

  const availableTakerTokenAmounts = new Map(await Promise.all(
    matches.map(async o => availableTakerTokenAmountAsync(dex, o)))
  )

  const fillableMatches = Order.availableForFill(matches, fillableMakerTokenAmounts, availableTakerTokenAmounts)

  const fillIntents = Order.matchIntentWithMakerOrders(
    tradeIntent, fillableMatches
  )

  const remainderMakerOrders = Order.makerOrderFromIntents(
    tradeIntent,
    fillIntents
  )

  const signedMakerOrders = await Promise.all(
    remainderMakerOrders.map(order => Order.sign(dex.mmWeb3, dex.contracts, order))
  )

  // check if the transaction hash can be calculated deterministically for creating fills
  const fillParams = await Promise.all(fillIntents.map(async i => {
    return {
      transactionHash: await Order.settle(dex.mmWeb3, dex.contracts, i),
      fillIntent: i
    }
  }))

  const fills = fillParams.map(Fill.create)
  const trade = Trade.create({tradeIntent, orders: signedMakerOrders, fills})

  addTradeToBlotter(dex, trade)

  await Promise.all(signedMakerOrders.map(o => broadcast(dex, o)))

  return trade
}

export async function getFillableMakerTokenAmountAsync (addr, tokenContract, proxyContract) {
  const tokenBalance = await balance(tokenContract, addr)
  const allowance = await tokenContract.methods.allowance(addr, address(proxyContract)).call()

  const fillableMakerTokenAmount = BigNumber.min(wad2D(tokenBalance), wad2D(allowance))

  return [addr, fillableMakerTokenAmount]
}

export async function availableTakerTokenAmountAsync (dex, order) {
  const unavailableTakerTokenAmount = await dex.contracts.xchg.methods.getUnavailableTakerTokenAmount(
    order.orderHash
  ).call()

  return [order.orderHash, order.buy.amount.minus(wad2D(unavailableTakerTokenAmount))]
}

async function preventOverOrder (dex, tradeIntent) {
  const {
    approved,
    hasBalance
  } = await userHasBalanceAndAllowance(dex, tradeIntent)

  const sellAmt = tradeIntent.sell.amount
  const sellSym = tradeIntent.sell.token.symbol

  if (approved !== true) {
    throw Error(`Approval required to sell \`${sellSym}\` token`)
  }

  if (hasBalance !== true) {
    throw Error(
      `Insufficient balance to sell ${sellAmt} \`${sellSym}\` token`
    )
  }
}

async function broadcast (dex, order) {
  dex.sendMessageChannel.next(
    toOrderMessage(order)
  )
}

async function cancelOrder (dex, order) {
  // If we cancel, we must be the maker.
  // cancelAmt is in taker token units.
  // Taker token is what the maker is buying.

  const intent = CancelIntent.create({
    order,
    cancelAmt: order.buy.amount // Cancel the full order.
  })

  return Order.cancel(dex.mmWeb3, dex.contracts, intent)
}
