#!/usr/bin/env bash
set -e

cd chain-dsl
pnpm i
cd ../dex
pnpm i

# overmind s
find contracts/ -name '*.sol' | bin/solc.sh || true

bin/import-keys.sh

bin/geth.sh dev none 3 &
bin/geth.sh dev2 none 3 &

bin/deploy.js http://localhost:8900 1337
bin/deploy.js http://localhost:8800 8
